﻿Imports System.Management
Imports System.IO

Namespace Processus

    Public Class Killer
        Implements IDisposable

#Region " Fields "
        Private ReadOnly m_pPath As String = String.Empty
        Private m_pList As List(Of Process)
#End Region

#Region " Methods "
        Public Sub New(pPath$)
            m_pPath = pPath
        End Sub

        Public Function IsRunning() As Boolean
            If GetProcessesByName().Count <> 0 Then
                Return True
            End If
            Return False
        End Function

        Public Function KillpTree() As Boolean
            For Each p As Process In m_pList
                If Not KillProcessTree(p) Then
                    Return False
                End If
            Next
            Return True
        End Function

        Private Function GetProcessesByName() As List(Of Process)
            m_pList = New List(Of Process)(Process.GetProcessesByName(Path.GetFileNameWithoutExtension(m_pPath)))
            Return m_pList
        End Function

        Private Function KillProcessTree(root As Process) As Boolean
            If root IsNot Nothing Then
                Dim list = New List(Of Process)()
                GetProcessAndChildren(Process.GetProcesses(), root, list, 1)
                For Each p As Process In list
                    Try
                        p.Kill()
                    Catch ex As Exception
                        Return False
                    End Try
                Next
            End If
            Return True
        End Function

        Private Function GetParentProcessId(p As Process) As Integer
            Dim parentId As Integer = 0
            Dim mo As New ManagementObject("win32_process.handle='" & Convert.ToString(p.Id) & "'")
            Try
                mo.Get()
                parentId = Convert.ToInt32(mo("ParentProcessId"))
            Catch ex As Exception
                parentId = 0
            Finally
                If Not mo Is Nothing Then mo.Dispose()
            End Try
            Return parentId
        End Function

        Private Sub GetProcessAndChildren(plist As Process(), parent As Process, output As List(Of Process), indent As Integer)
            For Each p As Process In plist
                If GetParentProcessId(p) = parent.Id Then
                    GetProcessAndChildren(plist, p, output, indent + 1)
                End If
            Next
            output.Add(parent)
        End Sub
#End Region

#Region " IDisposable "
        Private disposedValue As Boolean

        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                m_pList.Clear()
            End If
            Me.disposedValue = True
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
