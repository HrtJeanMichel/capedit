﻿Imports System.Runtime.InteropServices
Imports CapEdit.Win32

'sample by Tim Anderson http://www.itwriting.com/blog
'Email: tim@itwriting.com

'THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
'ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
'TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
'PARTICULAR PURPOSE.

'This sample is a C# implementation of some of the functions in 
'Andrei Belogortseff [ http://www.tweak-uac.com ]
'though IsReallyVista is nothing to do with Andrei

'The intention is to make it easy for .NET developers to discover whether or not 
'UAC is enabled and/or the current process is elevated


Namespace DragDrop
    Friend Class Elevated

        Friend Shared Function IsNT6orHigher() As Boolean
            Return (Environment.OSVersion.Version.Major >= 6)
        End Function

        '' <summary>
        ''
        ''The possible values are:

        ''TRUE - the current process is elevated.
        ''	This value indicates that either UAC is enabled, and the process was elevated by 
        ''	the administrator, or that UAC is disabled and the process was started by a user 
        ''	who is a member of the Administrators group.

        ''FALSE - the current process is not elevated (limited).
        ''	This value indicates that either UAC is enabled, and the process was started normally, 
        ''	without the elevation, or that UAC is disabled and the process was started by a standard user. 

        '' </summary>
        '' <returns>Bool indicating whether the current process is elevated</returns>
        Friend Shared Function IsElevated() As Boolean
            Dim bRetVal As Boolean
            Dim hToken As IntPtr = IntPtr.Zero
            Dim hProcess As IntPtr = NativeMethods.GetCurrentProcess()

            If hProcess = IntPtr.Zero Then
                Throw New ElevatedToolsException("Error getting current process handle")
            End If

            bRetVal = NativeMethods.OpenProcessToken(hProcess, NativeConstants.TOKEN_QUERY, hToken)


            If Not bRetVal Then
                Throw New ElevatedToolsException("Error opening process token")
            End If
            Try

                Dim te As NativeStructs.TOKEN_ELEVATION
                te.TokenIsElevated = 0

                Dim dwReturnLength As UInteger = 0
                Dim teSize As Integer = Marshal.SizeOf(te)
                Dim tePtr As IntPtr = Marshal.AllocHGlobal(teSize)
                Try
                    Marshal.StructureToPtr(te, tePtr, True)

                    bRetVal = NativeMethods.GetTokenInformation(hToken, NativeEnum.TOKEN_INFORMATION_CLASS.TokenElevation, tePtr, CType(teSize, UInteger), dwReturnLength)

                    If (Not bRetVal) Or (teSize <> dwReturnLength) Then
                        Throw New ElevatedToolsException("Error getting token information")
                    End If


                    te = CType(Marshal.PtrToStructure(tePtr, GetType(NativeStructs.TOKEN_ELEVATION)), NativeStructs.TOKEN_ELEVATION)
                Finally
                    Marshal.FreeHGlobal(tePtr)
                End Try


                Return (te.TokenIsElevated <> 0)
            Finally
                NativeMethods.CloseHandle(hToken)
            End Try

        End Function


        '' <summary>
        '' TokenElevationTypeDefault - User is not using a "split" token. 
        ''This value indicates that either UAC is disabled, or the process is started
        ''by a standard user (not a member of the Administrators group).

        ''The following two values can be returned only if both the UAC is enabled and
        ''the user is a member of the Administrator's group (that is, the user has a "split" token):

        ''TokenElevationTypeFull - the process is running elevated. 

        ''TokenElevationTypeLimited - the process is not running elevated.
        '' </summary>
        '' <returns>TokenElevationType</returns>
        Friend Shared Function GetElevationType() As NativeEnum.TOKEN_ELEVATION_TYPE
            'if (!IsReallyVista())
            '{
            '    throw new VistaToolsException("Function requires Vista or higher");
            '}

            Dim bRetVal As Boolean
            Dim hToken As IntPtr = IntPtr.Zero
            Dim hProcess As IntPtr = NativeMethods.GetCurrentProcess()

            If hProcess = IntPtr.Zero Then
                Throw New ElevatedToolsException("Error getting current process handle")
            End If

            bRetVal = NativeMethods.OpenProcessToken(hProcess, NativeConstants.TOKEN_QUERY, hToken)


            If Not bRetVal Then
                Throw New ElevatedToolsException("Error opening process token")
            End If
            Try

                Dim tet As NativeEnum.TOKEN_ELEVATION_TYPE = NativeEnum.TOKEN_ELEVATION_TYPE.TokenElevationTypeDefault

                Dim dwReturnLength As UInteger = 0
                Dim tetSize As UInteger = CUInt(System.Runtime.InteropServices.Marshal.SizeOf(CInt(tet)))
                Dim tetPtr As IntPtr = Marshal.AllocHGlobal(CInt(tetSize))
                Try

                    bRetVal = NativeMethods.GetTokenInformation(hToken, NativeEnum.TOKEN_INFORMATION_CLASS.TokenElevationType, tetPtr, tetSize, dwReturnLength)

                    If (Not bRetVal) Or (tetSize <> dwReturnLength) Then
                        Throw New ElevatedToolsException("Error getting token information")
                    End If

                    tet = CType(Marshal.ReadInt32(tetPtr), NativeEnum.TOKEN_ELEVATION_TYPE)
                Finally
                    Marshal.FreeHGlobal(tetPtr)
                End Try


                Return tet
            Finally
                NativeMethods.CloseHandle(hToken)
            End Try

        End Function
    End Class
End Namespace
