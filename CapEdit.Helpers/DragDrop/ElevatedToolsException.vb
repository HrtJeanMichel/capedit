﻿Namespace DragDrop
    Friend Class ElevatedToolsException
        Inherits ApplicationException
        ' Default constructor
        Friend Sub New()
        End Sub

        ' Constructor accepting a single string message
        Friend Sub New(message As String)
            MyBase.New(message)
        End Sub

        ' Constructor accepting a string message and an 
        ' inner exception which will be wrapped by this 
        ' custom exception class
        Friend Sub New(message As String, inner As Exception)
            MyBase.New(message, inner)
        End Sub
    End Class
End Namespace
