﻿Namespace DragDrop
    Public Class ElevatedDragDropArgs
        Inherits EventArgs
        Public Property HWnd() As IntPtr
        Public Property Files() As List(Of String)
        Public Property X() As Integer
        Public Property Y() As Integer
        Public Sub New()
            Files = New List(Of String)()
        End Sub
    End Class
End Namespace
