﻿Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports CapEdit.Win32

Namespace DragDrop
    Public Class ElevatedDragDropManager
        Implements IMessageFilter

#Region " Properties "
        Public Property HWnd As IntPtr
#End Region

#Region " Fields "
        Public Shared Instance As New ElevatedDragDropManager()
        Private ReadOnly IsVistaOrHigher As Boolean = Environment.OSVersion.Version.Major >= 6
        Private ReadOnly Is7OrHigher As Boolean = (Environment.OSVersion.Version.Major = 6 AndAlso Environment.OSVersion.Version.Minor >= 1) OrElse Environment.OSVersion.Version.Major > 6
#End Region

#Region " Events "
        Public Event ElevatedDragDrop As EventHandler(Of ElevatedDragDropArgs)
#End Region

#Region " Constructor "
        Protected Sub New()
            Application.AddMessageFilter(Me)
        End Sub
#End Region

#Region " Methods "
        Public Sub EnableDragDrop(hWnd As IntPtr)
            If Is7OrHigher Then
                Dim changeStruct As New NativeStructs.CHANGEFILTERSTRUCT With {
                    .cbSize = CUInt(Marshal.SizeOf(GetType(NativeStructs.CHANGEFILTERSTRUCT)))
                }
                NativeMethods.ChangeWindowMessageFilterEx(hWnd, NativeConstants.WM_DROPFILES, NativeEnum.ChangeWindowMessageFilterExAction.Allow, changeStruct)
                NativeMethods.ChangeWindowMessageFilterEx(hWnd, NativeConstants.WM_COPYDATA, NativeEnum.ChangeWindowMessageFilterExAction.Allow, changeStruct)
                NativeMethods.ChangeWindowMessageFilterEx(hWnd, NativeConstants.WM_COPYGLOBALDATA, NativeEnum.ChangeWindowMessageFilterExAction.Allow, changeStruct)
            ElseIf IsVistaOrHigher Then
                NativeMethods.ChangeWindowMessageFilter(NativeConstants.WM_DROPFILES, NativeEnum.ChangeWindowMessageFilterFlags.Add)
                NativeMethods.ChangeWindowMessageFilter(NativeConstants.WM_COPYDATA, NativeEnum.ChangeWindowMessageFilterFlags.Add)
                NativeMethods.ChangeWindowMessageFilter(NativeConstants.WM_COPYGLOBALDATA, NativeEnum.ChangeWindowMessageFilterFlags.Add)
            End If

            NativeMethods.DragAcceptFiles(hWnd, True)
        End Sub

        Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
            If m.Msg = NativeConstants.WM_DROPFILES Then
                HandleDragDropMessage(m)
                Return True
            End If
            Return False
        End Function

        Private Sub HandleDragDropMessage(m As Message)
            Dim sb = New StringBuilder(260)
            Dim numFiles As UInteger = NativeMethods.DragQueryFile(m.WParam, &HFFFFFFFFUI, sb, 0)
            Dim list = New List(Of String)()

            For i As UInteger = 0 To numFiles - 1
                If NativeMethods.DragQueryFile(m.WParam, i, sb, CUInt(sb.Capacity) * 2) > 0 Then
                    list.Add(sb.ToString())
                End If
            Next

            Dim p As NativeStructs.POINT
            NativeMethods.DragQueryPoint(m.WParam, p)
            NativeMethods.DragFinish(m.WParam)

            Dim args = New ElevatedDragDropArgs With {
                .HWnd = m.HWnd,
                .Files = list,
                .X = p.X,
                .Y = p.Y
            }

            RaiseEvent ElevatedDragDrop(Me, args)
        End Sub
#End Region

    End Class


End Namespace