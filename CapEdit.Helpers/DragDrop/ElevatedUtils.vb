﻿Imports System.IO
Imports CapEdit.Win32

Namespace DragDrop
    Public Class ElevatedUtils
        Public Shared Function isDirectory(filename$) As Boolean
            Return (File.GetAttributes(filename) And FileAttributes.Directory) = FileAttributes.Directory
        End Function

        Public Shared Function isElevated() As Boolean
            Return (Elevated.GetElevationType() = NativeEnum.TOKEN_ELEVATION_TYPE.TokenElevationTypeDefault)
        End Function
    End Class
End Namespace
