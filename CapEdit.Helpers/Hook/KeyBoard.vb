﻿Imports System.Text
Imports CapEdit.Win32

Namespace Hook
    Public Class KeyBoard

#Region " Fields "
        Private ReadOnly m_key1 As Integer
        Private ReadOnly m_key2 As Integer
        Private ReadOnly m_hWnd As IntPtr
        Private ReadOnly m_id As Integer

        Private Shared m_Keydict1 As Dictionary(Of String, Integer)
        Private Shared m_Keydict2 As Dictionary(Of String, Integer)
        Private Shared ReadOnly m_keyChars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#End Region

#Region " Constructor "
        Public Sub New(formHandle As IntPtr, key1 As Integer, key2 As Integer)
            m_hWnd = formHandle
            m_key1 = key1
            m_key2 = key2
            m_id = GetHashCode()
        End Sub

        Shared Sub New()
            AssociateKeys()
        End Sub

#End Region

#Region " Methods "
        Public Function RegisterKey() As Boolean
            Return NativeMethods.RegisterHotKey(m_hWnd, m_id, m_key1, m_key2)
        End Function

        Public Function UnregisterKey() As Boolean
            Return NativeMethods.UnregisterHotKey(m_hWnd, m_id)
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return m_key1 Xor m_key2 Xor m_hWnd.ToInt32()
        End Function

        Private Shared Sub AssociateKeys()
            m_Keydict1 = New Dictionary(Of String, Integer) From {{"Alt", NativeConstants.ALT}, {"Ctrl", NativeConstants.CTRL}, {"Shift", NativeConstants.SHIFT}, {"Win", NativeConstants.WIN}}
            m_Keydict2 = New Dictionary(Of String, Integer) From {{"Impecr", NativeConstants.IMPECR}, {"Insert", NativeConstants.INSERT}, {"Suppr", NativeConstants.SUPPR}}
            For Each c In m_keyChars
                m_Keydict2.Add(c, CInt(Encoding.ASCII.GetBytes(c)(0)))
            Next
        End Sub

        Private Shared Function GetKey(dic As Dictionary(Of String, Integer), StrKey$) As Integer
            Return dic.Item(StrKey)
        End Function

        Private Shared Function GetVal(dic As Dictionary(Of String, Integer), IntVal%) As String
            Return dic.FirstOrDefault(Function(x) x.Value = IntVal).Key
        End Function

        Public Shared Function GetShortCutKey1(StrKey$) As Integer
            Return GetKey(m_Keydict1, StrKey)
        End Function

        Public Shared Function GetShortCutKeyName1(IntVal%) As String
            Return GetVal(m_Keydict1, IntVal)
        End Function

        Public Shared Function GetShortCutKey2(StrKey$) As Integer
            Return GetKey(m_Keydict2, StrKey)
        End Function

        Public Shared Function GetShortCutKeyName2(IntVal%) As String
            Return GetVal(m_Keydict2, IntVal)
        End Function
#End Region

    End Class
End Namespace