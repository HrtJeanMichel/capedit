﻿Imports System.IO
Imports System.Drawing.Imaging
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D
Imports System.Windows.Media.Imaging
Imports System.Windows
Imports CapEdit.Win32

Namespace Pictures

    Public NotInheritable Class Utils

#Region " Fields "
        Private Shared ReadOnly m_ImageFormatdic As Dictionary(Of String, ImageFormat)
#End Region

#Region " Constructor "
        Shared Sub New()
            m_ImageFormatdic = New Dictionary(Of String, ImageFormat) From {
          {Extension.pngExt, Format.pngFormat},
          {Extension.bmpExt, Format.bmpFormat},
          {Extension.gifExt, Format.gifFormat},
          {Extension.jpegExt, Format.jpegFormat},
          {Extension.jpgExt, Format.jpegFormat},
          {Extension.tiffExt, Format.tiffFormat}}
        End Sub
#End Region

#Region " Methods "
        Public Shared Function GetImageFormat(Ext$) As ImageFormat
            Return m_ImageFormatdic.Item(Ext)
        End Function

        Private Shared Function GetGuidID(filepath$) As Guid
            Dim imagedata As Byte() = File.ReadAllBytes(filepath)
            Dim id As Guid
            Using ms As New MemoryStream(imagedata)
                Using img As Image = Image.FromStream(ms)
                    id = img.RawFormat.Guid
                End Using
            End Using
            Return id
        End Function

        Public Shared Function CorrectExtension(FilePath$) As Boolean
            Dim ext$ = New FileInfo(FilePath).Extension.ToLower
            If (ext = Extension.bmpExt) OrElse (ext = Extension.gifExt) OrElse (ext = Extension.jpgExt) OrElse
                    (ext = Extension.jpegExt) OrElse (ext = Extension.pngExt) OrElse (ext = Extension.tifExt) OrElse (ext = Extension.tiffExt) Then
                Return True
            End If
            Return False
        End Function

        Public Shared Function CorrectMIMEType(filepath$) As Boolean
            Try
                Dim id As Guid = GetGuidID(filepath)
                If id = ImageFormat.Png.Guid OrElse id = ImageFormat.Bmp.Guid OrElse
                    id = ImageFormat.Gif.Guid OrElse id = ImageFormat.Exif.Guid OrElse
                    id = ImageFormat.Jpeg.Guid OrElse id = ImageFormat.MemoryBmp.Guid OrElse
                    id = ImageFormat.Tiff.Guid Then
                    Return True
                End If
            Catch
                Return False
            End Try
            Return False
        End Function

        Public Shared Function GetMIMEType(filepath$) As String
            Dim mimeT As String = MimeType.m_unknown
            Try
                Select Case GetGuidID(filepath)
                    Case ImageFormat.Png.Guid
                        mimeT = MimeType.m_pngMime
                    Case ImageFormat.Bmp.Guid
                        mimeT = MimeType.m_bmpMime
                    Case ImageFormat.Gif.Guid
                        mimeT = MimeType.m_gifMime
                    Case ImageFormat.Exif.Guid
                        mimeT = MimeType.m_jpegMime
                    Case ImageFormat.Jpeg.Guid
                        mimeT = MimeType.m_jpegMime
                    Case ImageFormat.MemoryBmp.Guid
                        mimeT = MimeType.m_bmpMime
                    Case ImageFormat.Tiff.Guid
                        mimeT = MimeType.m_tiffMime
                End Select
            Catch
            End Try
            Return mimeT
        End Function

        Public Shared Function ImageToByteArray(image As Image) As Byte()
            Dim imageConverter As New ImageConverter()
            Dim imageByte As Byte() = DirectCast(imageConverter.ConvertTo(image, GetType(Byte())), Byte())
            Return imageByte
        End Function

        Public Shared Function ImageFromFile(path$) As Bitmap
            Dim bm As Bitmap
            Using img As Image = Image.FromFile(path)
                bm = New Bitmap(img)
            End Using
            Return bm
        End Function

        Public Shared Function BytestoString(Size As Long) As String
            Dim txt = String.Empty
            Try
                Dim i% = 1024
                Dim i1% = i * i
                Dim bl As Boolean = Size < CType(i, Long)
                If bl Then
                    If Size <= 1 Then
                        Return String.Concat(Size.ToString("D"), " octet")
                    Else
                        Return String.Concat(Size.ToString("D"), " octets")
                    End If
                Else
                    Dim dbl As Double = CType(Size, Double) / CType(i, Double)
                    bl = dbl < 1000
                    If bl Then
                        Dim dbl1 As Double = CType(Size, Double) / CType(i, Double)
                        Return String.Concat(dbl1.ToString("N"), " Ko")
                    Else
                        bl = dbl < 1000000
                        If bl Then
                            Dim dbl1 = CType(Size, Double) / CType(i1, Double)
                            Return String.Concat(dbl1.ToString("N"), " Mo")
                        Else
                            bl = dbl < 30000000
                            If bl Then
                                Dim dbl1 = (CType(Size, Double) / CType(i1, Double)) / CType(i, Double)
                                Return String.Concat(dbl1.ToString("N"), " Go")
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                txt = Size.ToString
                Return txt
            End Try
            Return txt
        End Function

        Public Shared Function ScaleImage(image As Image, maxWidth As Integer, maxHeight As Integer) As Image
            Dim transparentImg = New Bitmap(maxWidth, maxHeight, PixelFormat.Format32bppArgb)
            Dim x = (maxWidth / 2) - (image.Width / 2)
            Dim y = (maxHeight / 2) - (image.Height / 2)
            Using g = Graphics.FromImage(transparentImg)
                'g.Clear(Color.Transparent)
                g.Clear(Color.White)
            End Using

            Using grD As Graphics = Graphics.FromImage(transparentImg)
                Dim rect = New Rectangle(0, 0, image.Width, image.Height)
                Dim destrect = New Rectangle(x, y, image.Width, image.Height)
                grD.DrawImage(image, destrect, rect, GraphicsUnit.Pixel)
            End Using
            Return transparentImg
        End Function

        Public Shared Function DrawBitmapWithBorder(bmp As Bitmap, borderSize As Integer) As Bitmap
            Dim newWidth As Integer = bmp.Width + (borderSize * 2)
            Dim newHeight As Integer = bmp.Height + (borderSize * 2)
            Dim newImage As Image = New Bitmap(newWidth, newHeight)

            Using gfx As Graphics = Graphics.FromImage(newImage)

                Using border As Brush = New SolidBrush(Color.Black)
                    gfx.FillRectangle(border, 0, 0, newWidth, newHeight)
                End Using

                gfx.DrawImage(bmp, New Rectangle(borderSize, borderSize, bmp.Width, bmp.Height))
            End Using

            Return CType(newImage, Bitmap)
        End Function

        Public Shared Sub AutosizeImg(ImagePath$, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
            Try
                Pbx.Image = Nothing
                If File.Exists(ImagePath) Then
                    Dim img = ImageFromFile(ImagePath)
                    Pbx.Image = GetAutoSizeImage(img, Pbx.Size)
                    img.Dispose()
                Else
                    Pbx.Image = Nothing
                End If
                Pbx.SizeMode = pSizeMode
            Catch ex As Exception

            End Try
        End Sub

        Public Shared Sub AutosizeImg(Img As Image, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
            Try
                Pbx.Image = Nothing
                If Not Img Is Nothing Then
                    Pbx.Image = GetAutoSizeImage(Img, Pbx.Size)
                Else
                    Pbx.Image = Img
                End If
                Pbx.SizeMode = pSizeMode
            Catch ex As Exception
            End Try
        End Sub

        'Public Shared Sub SaveAsJpeg(img As Image, Filename$, Quality As Long)
        '    Dim oParams As New EncoderParameters(1)
        '    oParams.Param(0) = New EncoderParameter(Encoder.Quality, Quality)
        '    Dim oCodecInfo As ImageCodecInfo = GetEncoderInfoFromMimeType(MimeType.m_jpegMime)
        '    img.Save(Filename, oCodecInfo, oParams)
        'End Sub

        Public Shared Sub SavePicture(Img As Image, FileName$, ext As String, quality As Integer)
            Dim hbmp = DirectCast(Img, Bitmap).GetHbitmap
            Try
                Using fileStream = New FileStream(FileName, FileMode.Create)
                    Dim encoder As Object = Nothing
                    Select Case ext
                        Case Extension.bmpExt
                            encoder = New BmpBitmapEncoder
                        Case Extension.gifExt
                            encoder = New GifBitmapEncoder
                        Case Extension.jpgExt, Extension.jpegExt
                            encoder = New JpegBitmapEncoder With {
                                .QualityLevel = quality
                            }
                        Case Extension.pngExt
                            encoder = New PngBitmapEncoder
                        Case Extension.tifExt, Extension.tiffExt
                            encoder = New TiffBitmapEncoder
                    End Select
                    Dim bmpSrc = Interop.Imaging.CreateBitmapSourceFromHBitmap(hbmp, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions())
                    encoder.Frames.Add(BitmapFrame.Create(bmpSrc))
                    encoder.Save(fileStream)
                End Using
            Catch ex As Exception
                MsgBox(ex.ToString)
            Finally
                NativeMethods.DeleteObject(hbmp)
            End Try
        End Sub

        Public Shared Function GetGifFirstFrame(path$) As Bitmap
            Dim gif As New GifImage(path)
            Return gif.GetFrame(0)

            'Dim bitmap As Bitmap = Nothing
            'Try
            '    Dim gif = GifBitmapDecoder.Create(New Uri(path), BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default)
            '    Dim getFrames = gif.Frames(0)
            '    Using stream As New MemoryStream
            '        Dim enc As BitmapEncoder = New BmpBitmapEncoder()
            '        enc.Frames.Add(getFrames)
            '        enc.Save(stream)
            '        bitmap = New Bitmap(stream)

            '    End Using
            '    Return bitmap
            'Catch ex As Exception
            'Finally
            '    NativeMethods.DeleteObject(bitmap.GetHbitmap)
            'End Try
        End Function

        Private Shared Function GetAutoSizeImage(image As Image, size As Drawing.Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
            Dim newWidth% = image.Width
            Dim newHeight% = image.Height

            If image.Width > size.Width OrElse image.Height > size.Height Then
                If preserveAspectRatio Then
                    Dim originalWidth% = image.Width
                    Dim originalHeight% = image.Height
                    Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
                    Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
                    Dim percent As Single = If(percentHeight < percentWidth, percentHeight, percentWidth)
                    newWidth = CInt(originalWidth * percent)
                    newHeight = CInt(originalHeight * percent)
                Else
                    newWidth = size.Width
                    newHeight = size.Height
                End If
            End If

            Dim newImage As Image = New Bitmap(newWidth, newHeight)
            Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
            End Using

            Return newImage
        End Function
#End Region

    End Class

End Namespace