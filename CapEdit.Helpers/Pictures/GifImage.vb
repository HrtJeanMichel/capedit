﻿Imports System.Drawing
Imports System.Drawing.Imaging

Namespace Pictures
    Public Class GifImage

#Region " Fields "
        Private ReadOnly m_gifImage As Bitmap
        Private ReadOnly m_dimension As FrameDimension
        Private ReadOnly m_frameCount As Integer
        Private ReadOnly m_reverse As Boolean
        Private m_currentFrame As Integer = -1
        Private m_nextStep As Integer = 1
#End Region

#Region " Constructor "
        Public Sub New(path As String)
            m_gifImage = Utils.ImageFromFile(path)
            m_dimension = New FrameDimension(m_gifImage.FrameDimensionsList(0))
            m_frameCount = m_gifImage.GetFrameCount(m_dimension)
        End Sub
#End Region

#Region " Methods "
        Public Function GetNextFrame() As Bitmap
            m_currentFrame += m_nextStep
            If m_currentFrame >= m_frameCount OrElse m_currentFrame < 1 Then
                If m_reverse Then
                    m_nextStep *= -1
                    m_currentFrame += m_nextStep
                Else
                    m_currentFrame = 0
                End If
            End If
            Return GetFrame(m_currentFrame)
        End Function

        Public Function GetFrame(index%) As Bitmap
            m_gifImage.SelectActiveFrame(m_dimension, index)
            Return CType(m_gifImage.Clone(), Bitmap)
        End Function
#End Region

    End Class
End Namespace