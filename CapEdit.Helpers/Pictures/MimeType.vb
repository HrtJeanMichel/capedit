﻿Namespace Pictures

    Public NotInheritable Class MimeType
        Public Shared m_unknown$ = "image/unknown"
        Public Shared m_pngMime$ = "image/png"
        Public Shared m_bmpMime$ = "image/bmp"
        Public Shared m_gifMime$ = "image/gif"
        Public Shared m_jpegMime$ = "image/jpeg"
        Public Shared m_tiffMime$ = "image/tiff"
    End Class
End Namespace