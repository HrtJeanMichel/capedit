﻿Namespace Pictures
    Public NotInheritable Class Extension
        Public Shared pngExt As String = ".png"
        Public Shared bmpExt As String = ".bmp"
        Public Shared gifExt As String = ".gif"
        Public Shared jpegExt As String = ".jpeg"
        Public Shared jpgExt As String = ".jpg"
        Public Shared tiffExt As String = ".tiff"
        Public Shared tifExt As String = ".tif"
    End Class
End Namespace

