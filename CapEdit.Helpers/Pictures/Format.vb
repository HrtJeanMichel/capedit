﻿Imports System.Drawing.Imaging

Namespace Pictures
    Public NotInheritable Class Format
        Public Shared pngFormat As ImageFormat = ImageFormat.Png
        Public Shared bmpFormat As ImageFormat = ImageFormat.Bmp
        Public Shared gifFormat As ImageFormat = ImageFormat.Gif
        Public Shared jpegFormat As ImageFormat = ImageFormat.Jpeg
        Public Shared tiffFormat As ImageFormat = ImageFormat.Tiff
    End Class
End Namespace

