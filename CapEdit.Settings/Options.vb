﻿
<Serializable>
Public Class Options
    Property CaptureExtension As String = ".bmp"
    Property JpegCompression As Integer = 100
    Property HotKey1 As Integer = 4
    Property HotKey2 As Integer = 44
    Property ActiveWindow As Boolean = False
    Property AfterCapture As Boolean = False
    Property EditApp As String = My.Application.GetEnvironmentVariable("WINDIR") & "\System32\mspaint.exe"
    Property EditorDefault As Boolean = False
    Property EditorPenColor As String = "-65536"
    Property EditorPenSize As Single = 2
    Property GifDelay As Integer = 1200
    Property GifBorder As Boolean = False
    Sub New()
    End Sub
End Class
