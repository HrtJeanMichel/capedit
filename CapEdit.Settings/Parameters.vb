﻿Imports System.Xml.Serialization
Imports System.IO

<Serializable>
Public Class Parameters

#Region " Fields "
    Public Options As Options
    Private Shared m_appPath As String
#End Region

#Region " Constructor "
    Sub New()
        Options = New Options
    End Sub
#End Region

#Region " Methods "
    Public Shared Sub Initialize(appPath As String, ByRef aSettings As Parameters)
        m_appPath = appPath
        If Not Directory.Exists(appPath & "\CapEdit") Then Directory.CreateDirectory(appPath & "\CapEdit")
        If File.Exists(appPath & "\CapEdit\CapEdit-Settings.xml") Then
            aSettings = LoadFile()
        Else
            aSettings.SaveFile()
            aSettings = LoadFile()
        End If
    End Sub

    ''' <summary>
    ''' Enregistre l'état courant de la classe dans un fichier au format XML.
    ''' </summary>
    Public Sub SaveFile()
        Dim serializer As New XmlSerializer(GetType(Parameters))
        Dim sw As New StreamWriter(m_appPath & "\CapEdit\CapEdit-Settings.xml")
        serializer.Serialize(sw, Me)
        sw.Close()
    End Sub

    Public Shared Function LoadFile() As Parameters
        Dim deserializer As New XmlSerializer(GetType(Parameters))
        Dim sr As New StreamReader(m_appPath & "\CapEdit\CapEdit-Settings.xml")
        Dim p As Parameters = DirectCast(deserializer.Deserialize(sr), Parameters)
        sr.Close()
        Return p
    End Function
#End Region


End Class
