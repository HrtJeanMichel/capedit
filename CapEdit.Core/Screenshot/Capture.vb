﻿Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports CapEdit.Win32

Namespace Screenshot
    Friend Class Capture

#Region " Methods "

#Region " FullScreen "

        Friend Shared Function FullScreen() As Bitmap
            Return GetRectangle(SystemInformation.VirtualScreen)
        End Function

        Friend Shared Function SelectedMonitor(monitor As Screen) As Bitmap
            If monitor IsNot Nothing Then
                Return GetRectangle(monitor.Bounds)
            Else
                Throw New CaptureException("Le moniteur spécifié est introuvable !")
            End If
        End Function

#End Region

#Region " ActiveWindow "

        Public Shared Function ActiveWindow() As Bitmap
            Dim hwnd As IntPtr
            hwnd = NativeMethods.GetForegroundWindow()
            If hwnd <> IntPtr.Zero Then
                Return Control(hwnd)
            Else
                Throw New CaptureException("Il n'existe aucune fenêtre active !")
            End If
        End Function

        Private Overloads Shared Function Control(hwnd As IntPtr) As Bitmap
            Dim wRect As NativeStructs.RECT
            If hwnd <> IntPtr.Zero Then
                If Not NativeMethods.GetWindowRect(hwnd, wRect) Then
                    Dim eCode As Integer = Marshal.GetLastWin32Error
                    Throw New CaptureException(New Win32Exception(eCode).Message)
                Else
                    Return GetRectangle(wRect.ToRectangle)
                End If
            Else
                Throw New CaptureException("Handle de contrôle invalide !")
            End If
        End Function

#End Region

#Region " ScreenShot Helper "

        Private Shared Function GetRectangle(rect As Rectangle) As Image
            If Not rect.IsEmpty AndAlso rect.Width <> 0 AndAlso rect.Height <> 0 Then
                Dim bmp As Image = GetImage(rect)
                Return bmp
            Else
                Throw New CaptureException("Le rectangle spécifié est vide !")
            End If
        End Function

        Private Shared Function GetImage(rect As Rectangle) As Image
            Dim wHdc As IntPtr = GetDC()
            Dim g As Graphics
            Dim bmp As New Bitmap(rect.Width, rect.Height)
            g = Graphics.FromImage(bmp)
            Dim gHdc As IntPtr = g.GetHdc()
            If Not NativeMethods.BitBlt(gHdc, 0, 0, rect.Width, rect.Height, wHdc, rect.X, rect.Y, NativeConstants.SRCCOPY Or NativeConstants.CAPTUREBLT) Then
                Dim eCode As Integer = Marshal.GetLastWin32Error
                g.ReleaseHdc(gHdc)
                g.Dispose()
                Throw New CaptureException(New Win32Exception(eCode).Message)
            End If
            g.ReleaseHdc(gHdc)
            NativeMethods.ReleaseDC(IntPtr.Zero, wHdc)
            g.Dispose()
            If Not bmp Is Nothing Then
                Clipboard.SetImage(bmp)
            End If
            Return bmp
        End Function

        Private Shared Function GetDC() As IntPtr
            Dim wHdc As IntPtr = NativeMethods.GetDC(IntPtr.Zero)
            If wHdc = IntPtr.Zero Then
                Dim eCode As Integer = Marshal.GetLastWin32Error
                Throw New CaptureException(New Win32Exception(eCode).Message)
            Else
                Return wHdc
            End If
        End Function

#End Region

#End Region

    End Class
End Namespace
