﻿Imports System.ComponentModel
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms
Imports System.Drawing

Namespace Screenshot

    Friend Class CaptureArea
        Inherits Form

#Region " Fields "
        Private ReadOnly m_components As IContainer
        Private m_lastLoc As Point
        Private m_lastSize As Size
        Private m_mouseDownPoint As Point = Point.Empty
        Private m_mousePoint As Point = Point.Empty
        Private ReadOnly m_pen As Pen
        Private Shadows mouseDown As Boolean = False
        Private Shadows bounds As New Rectangle
#End Region

#Region " Properties "
        Friend Property ScreenBitmap As Image
#End Region

#Region " Constructor "
        Friend Sub New()
            InitializeComponent()

            TopMost = True
            DoubleBuffered = True

            For Each screen As Screen In Screen.AllScreens
                bounds = Rectangle.Union(bounds, screen.Bounds)
            Next

            Size = New Size(bounds.Width, bounds.Height)

            m_pen = New Pen(Color.Red, 2) With {
                .DashStyle = DashStyle.Solid
            }

            'Location = New Point(0, 0)

            'Dim maxX As Integer = 0
            'Dim maxY As Integer = 0

            'For Each screen As Screen In Screen.AllScreens
            '    Dim x As Integer = screen.Bounds.X + screen.Bounds.Width
            '    If x > maxX Then
            '        maxX = x
            '    End If
            '    Dim y As Integer = screen.Bounds.Y + screen.Bounds.Height
            '    If y > maxY Then
            '        maxY = y
            '    End If
            'Next

            'bounds.X = 0
            'bounds.Y = 0
            'bounds.Width = maxX
            'bounds.Height = maxY

            'Size = New Size(bounds.Width, bounds.Height)

            'm_pen = New Pen(Color.Red, 2)
            'm_pen.DashStyle = DashStyle.Solid


        End Sub
#End Region

#Region " Methods "
        Protected Overrides Sub Dispose(disposing As Boolean)
            If (disposing AndAlso (Not Me.m_components Is Nothing)) Then
                Me.m_components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Dim resources As ComponentResourceManager = New ComponentResourceManager(GetType(CaptureArea))
            Me.SuspendLayout()
            '
            'CaptureArea
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.BackColor = System.Drawing.Color.White
            Me.ClientSize = New System.Drawing.Size(1293, 214)
            Me.ControlBox = False
            Me.Cursor = System.Windows.Forms.Cursors.Cross
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Margin = New System.Windows.Forms.Padding(48, 22, 48, 22)
            Me.Name = "CaptureArea"
            Me.Opacity = 0.3R
            Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
            Me.Text = "CaptureArea"
            Me.TopMost = True
            Me.TransparencyKey = System.Drawing.Color.White
            Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
            Me.ResumeLayout(False)

        End Sub

        Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
            MyBase.OnMouseDown(e)
            Me.mouseDown = True
            Me.m_mousePoint = InlineAssignHelper(m_mouseDownPoint, e.Location)
        End Sub

        Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
            target = value
            Return value
        End Function

        Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
            MyBase.OnMouseUp(e)
            MyBase.Hide()
            Me.mouseDown = False
            Me.m_lastLoc = New Point(Math.Min(m_mouseDownPoint.X, m_mousePoint.X), Math.Min(m_mouseDownPoint.Y, m_mousePoint.Y))
            Me.m_lastSize = New Size(Math.Abs(m_mouseDownPoint.X - m_mousePoint.X), Math.Abs(m_mouseDownPoint.Y - m_mousePoint.Y))
            If m_lastSize.Width > 0 AndAlso m_lastSize.Height > 0 Then
                Dim r As New Rectangle With {
                    .Location = m_lastLoc,
                    .Size = m_lastSize
                }
                _ScreenBitmap = CaptureBitmap(r)
            End If
            MyBase.Close()
        End Sub

        Private Function CaptureBitmap(r As Rectangle) As Image
            Dim bitmape As Image = New Bitmap(r.Width, r.Height)
            Using g As Graphics = Graphics.FromImage(bitmape)
                g.CopyFromScreen(r.Location, New Point(0, 0), r.Size)
            End Using
            Return bitmape
        End Function

        Private Sub Cls_ScreenCapture_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
            m_mousePoint = e.Location
            Invalidate()
        End Sub

        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            Dim region As New Region(bounds)


            If mouseDown Then
                Dim selectionWindow As New Rectangle(Math.Min(m_mouseDownPoint.X, m_mousePoint.X), Math.Min(m_mouseDownPoint.Y, m_mousePoint.Y), Math.Abs(m_mouseDownPoint.X - m_mousePoint.X), Math.Abs(m_mouseDownPoint.Y - m_mousePoint.Y))
                region.Xor(selectionWindow)
                e.Graphics.FillRegion(Brushes.Black, region)

                m_pen.DashStyle = DashStyle.Dot
                e.Graphics.DrawRectangle(m_pen, selectionWindow)

                Dim str As String = (CStr(selectionWindow.Width) & " x " & CStr(selectionWindow.Height))
                Dim font As New Font("Segoe UI", 20, FontStyle.Regular)
                Dim sf As SizeF = e.Graphics.MeasureString(str, font)
                Dim x As Integer = CInt(Math.Round(CDbl(((selectionWindow.Location.X + selectionWindow.Width) - (sf.Width + 5)))))
                Dim loc As Point = selectionWindow.Location
                Dim y As Integer = CInt(Math.Round(CDbl(((loc.Y + selectionWindow.Height) - sf.Height))))
                Dim brush As New SolidBrush(Color.LightGray)
                loc = New Point(x, y)
                e.Graphics.DrawString(str, font, brush, CType(loc, PointF))
            Else
                e.Graphics.FillRegion(Brushes.LightGray, region)
                e.Graphics.DrawLine(m_pen, m_mousePoint.X, 0, m_mousePoint.X, Me.Size.Height)
                e.Graphics.DrawLine(m_pen, 0, m_mousePoint.Y, Me.Size.Width, m_mousePoint.Y)
            End If
        End Sub
#End Region

    End Class

End Namespace
