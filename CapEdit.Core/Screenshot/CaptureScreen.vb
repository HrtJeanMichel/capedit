﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing
Imports CapEdit.Helpers.Pictures

Namespace Screenshot
    Public Class CaptureScreen
        Implements IDisposable

#Region " Fields "
        Private m_ext As String = String.Empty
        Private ReadOnly m_jpgQuality As Integer = 100
        Public ScreenPath As String = String.Empty
        Public ScreenBitmap As Bitmap = Nothing
#End Region

#Region " Events "
        Public Event CaptureTerminated(sender As Object, e As EventArgs)
#End Region

#Region " Enumeration "
        Public Enum ShotType
            Full = 0
            SelectedMonitor = 1
            Active = 2
            SelectedArea = 3
        End Enum
#End Region

#Region " Constructor "
        Public Sub New(ext$, Optional ByVal JpgQuality As Integer = 100)
            m_ext = ext
            m_jpgQuality = JpgQuality
            ScreenPath = GeneratedFile()
        End Sub
#End Region

#Region " Methods "
        Private Sub CaptureTerminatedEventRaiser()
            RaiseEvent CaptureTerminated(Me, New EventArgs())
        End Sub

        Private Function GeneratedFile() As String
            Return Path.Combine(Path.GetTempPath, Date.Now.ToString("yyyyMMddHHmmssffffff") & m_ext)
        End Function

        Public Sub Shot(Type As ShotType, Optional ByVal arg As Screen = Nothing)
            Select Case Type
                Case ShotType.Full
                    ScreenBitmap = Capture.FullScreen()
                    SaveScreenBitmap()
                Case ShotType.SelectedMonitor
                    ScreenBitmap = Capture.SelectedMonitor(arg)
                    SaveScreenBitmap()
                Case ShotType.Active
                    ScreenBitmap = Capture.ActiveWindow()
                    SaveScreenBitmap()
                Case ShotType.SelectedArea
                    Dim ca As CaptureArea = Nothing
                    Try
                        ca = New CaptureArea()
                        ca.ShowDialog()
                        ScreenBitmap = ca.ScreenBitmap

                        If Not ScreenBitmap Is Nothing Then
                            SaveScreenBitmap()
                        End If
                    Catch ex As Exception
                    Finally
                        If Not ca Is Nothing Then ca.Dispose()
                    End Try
            End Select
            CaptureTerminatedEventRaiser()
        End Sub

        Private Sub SaveScreenBitmap()
            Utils.SavePicture(ScreenBitmap, ScreenPath, m_ext, m_jpgQuality)
        End Sub
#End Region

#Region " IDisposable "
        Private disposedValue As Boolean

        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    If Not ScreenBitmap Is Nothing Then ScreenBitmap.Dispose()
                End If
                ScreenPath = String.Empty
                m_ext = String.Empty
            End If
            disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace