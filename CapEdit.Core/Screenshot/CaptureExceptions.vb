﻿Namespace Screenshot
    Friend Class CaptureException
        Inherits Exception

        Friend Sub New(message$)
            MyBase.New(message)
        End Sub

        Friend Sub New(message$, ex As Exception)
            MyBase.New(message, ex)
        End Sub

        Protected Sub New(info As Runtime.Serialization.SerializationInfo, context As Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class
End Namespace
