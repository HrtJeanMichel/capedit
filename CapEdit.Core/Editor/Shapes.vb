﻿Imports System.Collections.ObjectModel
Imports System.Collections.Specialized

Namespace Editor
    Public Class Shapes(Of T)
        Inherits ObservableCollection(Of T)

        Public Event ItemAdded As EventHandler(Of NotifyCollectionChangedEventArgs)
        Public Event ItemRemoved As EventHandler(Of NotifyCollectionChangedEventArgs)
        Public Event ItemReset As EventHandler(Of NotifyCollectionChangedEventArgs)

        Public Sub New()
            AddHandler CollectionChanged, AddressOf Rectangles_CollectionChanged
        End Sub

        Private Sub Rectangles_CollectionChanged(sender As Object, e As NotifyCollectionChangedEventArgs)
            If e.Action = NotifyCollectionChangedAction.Add Then
                RaiseEvent ItemAdded(sender, e)
            ElseIf e.Action = NotifyCollectionChangedAction.Remove Then
                RaiseEvent ItemRemoved(sender, e)
            ElseIf e.Action = NotifyCollectionChangedAction.Reset Then
                RaiseEvent ItemReset(sender, e)
            End If
        End Sub
    End Class
End Namespace
