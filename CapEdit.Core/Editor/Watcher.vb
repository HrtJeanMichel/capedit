﻿Imports System.IO
Imports System.Management
Imports System.Runtime.CompilerServices

Namespace Editor
    Public Class Watcher

#Region " Delegates "
        Public Delegate Sub StartedEventHandler(sender As Object, e As EventArgs)
        Public Delegate Sub TerminatedEventHandler(sender As Object, e As EventArgs)
        Public Delegate Sub StoppedEventHandler(sender As Object, e As EventArgs)
#End Region

#Region " Fields "
        Public Started As StartedEventHandler = Nothing
        Public Terminated As TerminatedEventHandler = Nothing
        Public Stopped As StoppedEventHandler = Nothing

        Private m_watcher As ManagementEventWatcher = Nothing
        Private m_oProcess As Process = Nothing
        Private m_appPath$ = String.Empty
#End Region

#Region " Methods "

        Public Sub StartWatch(appPath$, imagePath$)
            m_appPath = appPath
            Dim oScope As New ManagementScope("root\CIMV2")
            oScope.Options.EnablePrivileges = True
            Dim oQuery As New EventQuery("SELECT * FROM __InstanceOperationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_Process' AND TargetInstance.Name = '" & New FileInfo(m_appPath).Name & "'")
            m_watcher = New ManagementEventWatcher(oScope, oQuery)
            AddHandler m_watcher.EventArrived, New EventArrivedEventHandler(AddressOf OnEventArrived)
            Dim handler1 As StoppedEventHandler = New StoppedEventHandler(AddressOf convertible)
            AddHandler m_watcher.Stopped, New System.Management.StoppedEventHandler(AddressOf handler1.Invoke)
            m_watcher.Start()
            m_oProcess = New Process()
            m_oProcess.StartInfo.FileName = appPath
            m_oProcess.StartInfo.Arguments = Chr(34) & imagePath & Chr(34)
            m_oProcess.Start()
        End Sub

        Private Sub Convertible(a0 As Object, a1 As EventArgs)
            OnEventStopped(RuntimeHelpers.GetObjectValue(a0), DirectCast(a1, StoppedEventArgs))
        End Sub
        Public Sub StopWatch()
            If Not m_watcher Is Nothing Then
                RemoveHandler m_watcher.EventArrived, AddressOf OnEventArrived
                RemoveHandler m_watcher.Stopped, AddressOf OnEventStopped
                m_watcher.Stop()
            End If
        End Sub

        Private Sub OnEventArrived(sender As Object, e As System.Management.EventArrivedEventArgs)
            Try
                Dim eventName As String = e.NewEvent.ClassPath.ClassName
                If eventName.CompareTo("__InstanceCreationEvent") = 0 Then
                    If (Not Started Is Nothing) Then
                        Started.Invoke(Me, e)
                    End If
                ElseIf eventName.CompareTo("__InstanceDeletionEvent") = 0 Then
                    If (Not Terminated Is Nothing) Then
                        Terminated.Invoke(Me, e)
                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub

        Private Sub OnEventStopped(sender As Object, e As System.Management.StoppedEventArgs)
            If (Not Stopped Is Nothing) Then
                Stopped.Invoke(Me, e)
            End If
        End Sub
#End Region

#Region " Disposable "
        Public Sub Dispose()
            If Not m_watcher Is Nothing Then m_watcher.Dispose()
            If Not m_oProcess Is Nothing Then m_oProcess.Dispose()
        End Sub
#End Region

    End Class
End Namespace
