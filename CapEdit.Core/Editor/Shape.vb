﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Namespace Editor
    Public Class Shape

        Public Property PenSize As pensize
        Public Property Color As Color
        Public Property GraphicsPath As GraphicsPath

        Public Sub New(sColor As Color, sPenSize As PenSize, sGraphicsPath As GraphicsPath)
            _Color = sColor
            _PenSize = sPenSize
            _GraphicsPath = sGraphicsPath
        End Sub

        Public Sub New()
        End Sub
    End Class
End Namespace
