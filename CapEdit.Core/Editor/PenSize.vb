﻿Namespace Editor
    Public Enum PenSize
        Mini = 1
        Small = 2
        Big = 3
        Xxl = 4
        Xxxl = 5
    End Enum
End Namespace
