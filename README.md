# **CapEdit** #

# Description

Cet outil a été codé dans le but de gagner du temps lors de la rédaction de tutoriels qui nécessitent des captures d'écrans.
Chaque capture est ajoutée dans une liste de fichiers et peuvent ainsi être rapidement modifiées depuis votre logiciel de retouche d'image ou l'éditeur intégré.
Il est également possible de générer un fichier animé au format GIF à partir d'une sélection d'images (jpg, bmp, png, gif).

# Screenshot




# Features

* Ajout de fichiers : jpg, bmp, png, gif
* Prévisualisation des images de la liste
* Ordonnancement de la liste des fichiers par bouton
* Paramétrage du délai d'animation
* Ajout ou non d'un contour sur chaque image avant la génération du GIF (pratique pour les images blanches sur fond blanc)


# Prerequisites

* Tout OS Windows
* DotNet Framework 4.5
* Pas d'installation


# WebSite




# Credits

* Cyotek : Pour l'extension du contrôle ListView [Dragging ListviewItem](http://www.cyotek.com/blog/dragging-items-in-a-listview-control-with-visual-insertion-guides)
* Julien Roncaglia : Pour sa classe de gestion des icones [IconHandler](https://stackoverflow.com/users/story/46594)


# Copyright

Copyright © HrtJm 2008-2020


# Licence

