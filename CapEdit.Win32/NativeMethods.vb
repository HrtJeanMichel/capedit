﻿Imports System.Runtime.InteropServices
Imports System.Text

Public Class NativeMethods

#Region " CapEdit.Components.ListViewEx "
    <DllImport("user32.dll")>
    Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wPar As IntPtr, lPar As IntPtr) As IntPtr
    End Function

    <DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)>
    Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
    End Function
#End Region

#Region " CapEdit.Core.Capture "
    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function GetWindowRect(hWnd As IntPtr, ByRef lpRect As NativeStructs.RECT) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function ReleaseDC(hWnd As IntPtr, hdc As IntPtr) As Integer
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function WindowFromPoint(pt As NativeStructs.POINT) As IntPtr
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function GetForegroundWindow() As IntPtr
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function GetAncestor(hwnd As IntPtr, gaFlags As Integer) As IntPtr
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Public Shared Function GetDC(hwnd As IntPtr) As IntPtr
    End Function

    <DllImport("gdi32.dll", SetLastError:=True)>
    Public Shared Function BitBlt(ByVal hdcDest As IntPtr, ByVal nXDest As Integer, ByVal nYDest As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer,
                                    ByVal hdcSrc As IntPtr, ByVal nXSrc As Integer, ByVal nYSrc As Integer, ByVal dwRop As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
#End Region

#Region " CapEdit.Core.HookKey "
    <DllImport("user32.dll")>
    Public Shared Function RegisterHotKey(hWnd As IntPtr, id As Integer, fsModifiers As Integer, vk As Integer) As Boolean
    End Function

    <DllImport("user32.dll")>
    Public Shared Function UnregisterHotKey(hWnd As IntPtr, id As Integer) As Boolean
    End Function
#End Region

#Region " CapEdit.Helpers.Pictures.Icons "
    <DllImport("Shell32", CharSet:=CharSet.Auto)>
    Public Shared Function ExtractIconEx(<MarshalAs(UnmanagedType.LPTStr)> lpszFile As String, nIconIndex As Integer, phIconLarge As IntPtr(), phIconSmall As IntPtr(), nIcons As Integer) As Integer
    End Function

    <DllImport("Shell32", CharSet:=CharSet.Auto)>
    Public Shared Function SHGetFileInfo(pszPath As String, dwFileAttributes As Integer, psfi As NativeStructs.SHFILEINFO, cbFileInfo As Integer, uFlags As NativeEnum.SHGFI) As IntPtr
    End Function
#End Region

#Region " CapEdit.Helpers.Pictures.Utils "
    <DllImport("gdi32.dll")>
    Public Shared Function DeleteObject(hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function
#End Region

    '#Region " CapEdit.Helpers.DragDrop.Elevated "
    '    <DllImport("advapi32.dll", SetLastError:=True)>
    '    Public Shared Function OpenProcessToken(ProcessHandle As IntPtr, DesiredAccess As UInteger, ByRef TokenHandle As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    '    End Function

    '    <DllImport("kernel32.dll", SetLastError:=True)>
    '    Public Shared Function GetCurrentProcess() As IntPtr
    '    End Function

    '    <DllImport("advapi32.dll", SetLastError:=True)>
    '    Public Shared Function GetTokenInformation(TokenHandle As IntPtr, TokenInformationClass As NativeEnum.TOKEN_INFORMATION_CLASS, TokenInformation As IntPtr, TokenInformationLength As UInteger, ByRef ReturnLength As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    '    End Function

    '    <DllImport("kernel32.dll", SetLastError:=True)>
    '    Public Shared Function CloseHandle(hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    '    End Function

    '    <DllImport("kernel32.dll", CharSet:=CharSet.Ansi, ExactSpelling:=True)>
    '    Public Shared Function GetProcAddress(hmodule As IntPtr, procName As String) As IntPtr
    '    End Function
    '#End Region

    '#Region " CapEdit.Helpers.DragDrop.ElevatedDragDropManager "
    '    <DllImport("user32.dll")>
    '    Public Shared Function ChangeWindowMessageFilterEx(hWnd As IntPtr, msg As UInteger, action As NativeEnum.ChangeWindowMessageFilterExAction, ByRef changeInfo As NativeStructs.CHANGEFILTERSTRUCT) As <MarshalAs(UnmanagedType.Bool)> Boolean
    '    End Function

    '    <DllImport("user32.dll")>
    '    Public Shared Function ChangeWindowMessageFilter(msg As UInteger, flags As NativeEnum.ChangeWindowMessageFilterFlags) As <MarshalAs(UnmanagedType.Bool)> Boolean
    '    End Function

    '    <DllImport("shell32.dll")>
    '    Public Shared Sub DragAcceptFiles(hwnd As IntPtr, fAccept As Boolean)
    '    End Sub

    '    <DllImport("shell32.dll")>
    '    Public Shared Function DragQueryFile(hDrop As IntPtr, iFile As UInteger, <Out()> lpszFile As StringBuilder, cch As UInteger) As UInteger
    '    End Function

    '    <DllImport("shell32.dll")>
    '    Public Shared Function DragQueryPoint(hDrop As IntPtr, ByRef lppt As NativeStructs.POINT) As Boolean
    '    End Function

    '    <DllImport("shell32.dll")>
    '    Public Shared Sub DragFinish(hDrop As IntPtr)
    '    End Sub
    '#End Region

End Class
