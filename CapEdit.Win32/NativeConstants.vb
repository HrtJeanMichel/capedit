﻿Public Class NativeConstants

#Region " CapEdit.Components.ListViewEx "
    Public Const LVM_FIRST As Integer = 4096
    Public Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
    Public Const LVS_EX_DOUBLEBUFFER As Integer = 65536
    Public Const WM_PAINT As Integer = 15
#End Region

#Region " CapEdit.Core.Capture "
    'GDI32
    Public Const CAPTUREBLT As Integer = &H40000000
    Public Const SRCCOPY As Integer = &HCC0020
    'ANCESTOR FLAGS
    Public Const GA_ROOTOWNER As Integer = 3
#End Region

#Region " CapEdit.Core.HookKey "
    'FSMODIFIERS
    Public Const NO As Integer = 0
    Public Const ALT As Integer = 1
    Public Const CTRL As Integer = 2
    Public Const SHIFT As Integer = 4
    Public Const WIN As Integer = 8
    'VIRTUALKEYS
    Public Const IMPECR As Integer = 44
    Public Const INSERT As Integer = 45
    Public Const SUPPR As Integer = 46

    Public Const WM_HOTKEY As Integer = 786
#End Region

    '#Region " CapEdit.Helpers.DragDrop "
    '    Public Const STANDARD_RIGHTS_REQUIRED As UInteger = &HF0000
    '    Public Const TOKEN_ASSIGN_PRIMARY As UInteger = &H1
    '    Public Const TOKEN_DUPLICATE As UInteger = &H2
    '    Public Const TOKEN_IMPERSONATE As UInteger = &H4
    '    Public Const TOKEN_QUERY As UInteger = &H8
    '    Public Const TOKEN_QUERY_SOURCE As UInteger = &H10
    '    Public Const TOKEN_ADJUST_PRIVILEGES As UInteger = &H20
    '    Public Const TOKEN_ADJUST_GROUPS As UInteger = &H40
    '    Public Const TOKEN_ADJUST_DEFAULT As UInteger = &H80
    '    Public Const TOKEN_ADJUST_SESSIONID As UInteger = &H100

    '    Public Const TOKEN_ALL_ACCESS_P As UInteger = (STANDARD_RIGHTS_REQUIRED Or TOKEN_ASSIGN_PRIMARY Or TOKEN_DUPLICATE Or TOKEN_IMPERSONATE Or TOKEN_QUERY Or TOKEN_QUERY_SOURCE Or TOKEN_ADJUST_PRIVILEGES Or TOKEN_ADJUST_GROUPS Or TOKEN_ADJUST_DEFAULT)
    '#End Region

    '#Region " CapEdit.Helpers.DragDrop.ElevatedDragDropManager "
    '    Public Const WM_DROPFILES As UInteger = &H233
    '    Public Const WM_COPYDATA As UInteger = &H4A
    '    Public Const WM_COPYGLOBALDATA As UInteger = &H49
    '#End Region

End Class
