﻿Public NotInheritable Class NativeEnum

#Region " CapEdit.Helpers.Pictures.Icons "
    <Flags>
    Public Enum SHGFI
        SHGFI_ICON = &H100
        SHGFI_DISPLAYNAME = &H200
        SHGFI_TYPENAME = &H400
        SHGFI_ATTRIBUTES = &H800
        SHGFI_ICONLOCATION = &H1000
        SHGFI_EXETYPE = &H2000
        SHGFI_SYSICONINDEX = &H4000
        SHGFI_LINKOVERLAY = &H8000
        SHGFI_SELECTED = &H10000
        SHGFI_ATTR_SPECIFIED = &H20000
        SHGFI_LARGEICON = &H0
        SHGFI_SMALLICON = &H1
        SHGFI_OPENICON = &H2
        SHGFI_SHELLICONSIZE = &H4
        SHGFI_PIDL = &H8
        SHGFI_USEFILEATTRIBUTES = &H10
        SHGFI_ADDOVERLAYS = &H20
        SHGFI_OVERLAYINDEX = &H40
        FILE_ATTRIBUTE_NORMAL = &H80
    End Enum
#End Region

    '#Region " CapEdit.Helpers.DragDrop.ElevatedDragDropManager "

    '    Public Enum TOKEN_INFORMATION_CLASS
    '        TokenUser = 1
    '        TokenGroups = 2
    '        TokenPrivileges = 3
    '        TokenOwner = 4
    '        TokenPrimaryGroup = 5
    '        TokenDefaultDacl = 6
    '        TokenSource = 7
    '        TokenType = 8
    '        TokenImpersonationLevel = 9
    '        TokenStatistics = 10
    '        TokenRestrictedSids = 11
    '        TokenSessionId = 12
    '        TokenGroupsAndPrivileges = 13
    '        TokenSessionReference = 14
    '        TokenSandBoxInert = 15
    '        TokenAuditPolicy = 16
    '        TokenOrigin = 17
    '        TokenElevationType = 18
    '        TokenLinkedToken = 19
    '        TokenElevation = 20
    '        TokenHasRestrictions = 21
    '        TokenAccessInformation = 22
    '        TokenVirtualizationAllowed = 23
    '        TokenVirtualizationEnabled = 24
    '        TokenIntegrityLevel = 25
    '        TokenUIAccess = 26
    '        TokenMandatoryPolicy = 27
    '        TokenLogonSid = 28
    '        MaxTokenInfoClass = 29
    '    End Enum

    '    Public Enum TOKEN_ELEVATION_TYPE
    '        TokenElevationTypeDefault = 1
    '        TokenElevationTypeFull = 2
    '        TokenElevationTypeLimited = 3
    '    End Enum

    '    Public Enum ChangeWindowMessageFilterExAction As UInteger
    '        Reset
    '        Allow
    '        Disallow
    '    End Enum

    '    Public Enum ChangeWindowMessageFilterFlags As UInteger
    '        Add = 1
    '        Remove = 2
    '    End Enum

    '    Public Enum MessageFilterInfo As UInteger
    '        None
    '        AlreadyAllowed
    '        AlreadyDisAllowed
    '        AllowedHigher
    '    End Enum
    '#End Region

End Class