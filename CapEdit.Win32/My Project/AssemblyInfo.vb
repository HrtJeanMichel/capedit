﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Vérifiez les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("CapEdit.Win32")>
<Assembly: AssemblyDescription("CapEdit.Win32")>
<Assembly: AssemblyCompany("Hrtjm")>
<Assembly: AssemblyProduct("CapEdit.Win32")>
<Assembly: AssemblyCopyright("Copyright © Hrtjm 2008-2020")>
<Assembly: AssemblyTrademark("Hrtjm")>

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("6002e233-6e15-47e9-89e4-5a837e12c009")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
