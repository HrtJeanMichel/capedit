﻿Imports System.Runtime.InteropServices
Imports System.Drawing

Public NotInheritable Class NativeStructs

#Region " CapEdit.Helpers.Pictures.Icons "
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Public Structure SHFILEINFO
        Public hIcon As IntPtr
        Public iIcon As Integer
        Public dwAttributes As UInteger
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=260)>
        Public szDisplayName As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)>
        Public szTypeName As String
    End Structure
#End Region

#Region " CapEdit.Core.Capture "
    <StructLayout(LayoutKind.Sequential)>
    Public Structure POINT
        Public X As Integer
        Public Y As Integer

        Public Sub New(newX As Integer, newY As Integer)
            X = newX
            Y = newY
        End Sub

        Public Shared Widening Operator CType(p As POINT) As Drawing.Point
            Return New Drawing.Point(p.X, p.Y)
        End Operator

        Public Shared Widening Operator CType(p As Drawing.Point) As POINT
            Return New POINT(p.X, p.Y)
        End Operator
    End Structure

    Public Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer

        Public Function ToRectangle() As Rectangle
            Return New Rectangle(Me.Left, Me.Top, Me.Right - Me.Left, Me.Bottom - Me.Top)
        End Function
    End Structure
#End Region

    '#Region " CapEdit.Helpers.DragDrop.ElevatedDragDropManager "
    '    <StructLayout(LayoutKind.Sequential)>
    '    Public Structure CHANGEFILTERSTRUCT
    '        Public cbSize As UInteger
    '        Public ExtStatus As NativeEnum.MessageFilterInfo
    '    End Structure

    '    Public Structure TOKEN_ELEVATION
    '        Public TokenIsElevated As UInteger
    '    End Structure


    '#End Region
End Class
