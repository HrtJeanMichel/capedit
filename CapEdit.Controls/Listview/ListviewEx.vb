﻿Imports System.Windows.Forms
Imports CapEdit.Win32

Public Class ListviewEx
    Inherits ListView

#Region " Fields "
    Private m_elv As Boolean = False
#End Region

#Region " Events "
    Public Event ItemChanged As EventHandler
#End Region

    Public Function AddItem(Text As String) As ListViewItem
        Dim lvi = MyBase.Items.Add(Text)
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
        Return lvi
    End Function

    Public Sub ItemsAddRange(Item As ListViewItem())
        MyBase.Items.AddRange(Item)
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
    End Sub

    Public Sub RemoveLvi(item As ListViewItem)
        MyBase.Items.Remove(item)
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
    End Sub

    Public Sub RemoveItem(Item As ListViewItem)
        MyBase.Items.Remove(Item)
        RaiseEvent ItemChanged(Me, EventArgs.Empty)
    End Sub

#Region " Methods "
    Private Sub SetWindowTheme()
        NativeMethods.SetWindowTheme(Handle, "explorer", Nothing)
        NativeMethods.SendMessage(Handle, NativeConstants.LVM_SETEXTENDEDLISTVIEWSTYLE, NativeConstants.LVS_EX_DOUBLEBUFFER, NativeConstants.LVS_EX_DOUBLEBUFFER)
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)
        Select Case m.Msg
            Case NativeConstants.WM_PAINT
                If Not m_elv Then
                    SetWindowTheme()
                    m_elv = True
                End If
                Exit Select
        End Select
        MyBase.WndProc(m)
    End Sub

#End Region

End Class

