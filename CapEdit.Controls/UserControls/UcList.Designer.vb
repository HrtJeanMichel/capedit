﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcList
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UcList))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TsList = New System.Windows.Forms.ToolStrip()
        Me.TsBtnListCapture = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnListAddFile = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnListRemoveFile = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnListEdit = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnListSaveAs = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnListUp = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnListDown = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnListSaveAnimation = New System.Windows.Forms.ToolStripButton()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.ImageListList = New System.Windows.Forms.ImageList(Me.components)
        Me.GbxPreview = New System.Windows.Forms.GroupBox()
        Me.PcbListPicture = New System.Windows.Forms.PictureBox()
        Me.BgwAddFiles = New System.ComponentModel.BackgroundWorker()
        Me.BgwGifAnimationStart = New System.ComponentModel.BackgroundWorker()
        Me.LvList = New CapEdit.Controls.ListviewEx()
        Me.ClhListName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhListPath = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhListSize = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TsList.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GbxPreview.SuspendLayout()
        CType(Me.PcbListPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TsList)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Size = New System.Drawing.Size(594, 444)
        Me.SplitContainer1.SplitterDistance = 25
        Me.SplitContainer1.TabIndex = 16
        '
        'TsList
        '
        Me.TsList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TsList.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsList.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.TsList.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnListCapture, Me.ToolStripSeparator1, Me.TsBtnListAddFile, Me.TsBtnListRemoveFile, Me.ToolStripSeparator2, Me.TsBtnListEdit, Me.TsBtnListSaveAs, Me.TsBtnListUp, Me.ToolStripSeparator3, Me.TsBtnListDown, Me.TsBtnListSaveAnimation})
        Me.TsList.Location = New System.Drawing.Point(0, 0)
        Me.TsList.Name = "TsList"
        Me.TsList.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.TsList.Size = New System.Drawing.Size(594, 25)
        Me.TsList.TabIndex = 0
        Me.TsList.Text = "TabPage0"
        '
        'TsBtnListCapture
        '
        Me.TsBtnListCapture.Image = CType(resources.GetObject("TsBtnListCapture.Image"), System.Drawing.Image)
        Me.TsBtnListCapture.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListCapture.Name = "TsBtnListCapture"
        Me.TsBtnListCapture.Size = New System.Drawing.Size(81, 22)
        Me.TsBtnListCapture.Tag = "0"
        Me.TsBtnListCapture.Text = "Capturer"
        Me.TsBtnListCapture.ToolTipText = "Sélectionner une zone en vue de la capturer"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnListAddFile
        '
        Me.TsBtnListAddFile.Image = CType(resources.GetObject("TsBtnListAddFile.Image"), System.Drawing.Image)
        Me.TsBtnListAddFile.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListAddFile.Name = "TsBtnListAddFile"
        Me.TsBtnListAddFile.Size = New System.Drawing.Size(74, 22)
        Me.TsBtnListAddFile.Text = "Ajouter"
        Me.TsBtnListAddFile.ToolTipText = "Ajouter vos fichiers dans la file d'attente pour les modifier et les enregistrer " &
    "ultérieurement"
        '
        'TsBtnListRemoveFile
        '
        Me.TsBtnListRemoveFile.Enabled = False
        Me.TsBtnListRemoveFile.Image = CType(resources.GetObject("TsBtnListRemoveFile.Image"), System.Drawing.Image)
        Me.TsBtnListRemoveFile.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListRemoveFile.Name = "TsBtnListRemoveFile"
        Me.TsBtnListRemoveFile.Size = New System.Drawing.Size(90, 22)
        Me.TsBtnListRemoveFile.Text = "Supprimer"
        Me.TsBtnListRemoveFile.ToolTipText = "Supprimer le/les fichiers de la liste d'attente"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnListEdit
        '
        Me.TsBtnListEdit.Enabled = False
        Me.TsBtnListEdit.Image = CType(resources.GetObject("TsBtnListEdit.Image"), System.Drawing.Image)
        Me.TsBtnListEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListEdit.Name = "TsBtnListEdit"
        Me.TsBtnListEdit.Size = New System.Drawing.Size(65, 22)
        Me.TsBtnListEdit.Tag = "1"
        Me.TsBtnListEdit.Text = "Editer"
        Me.TsBtnListEdit.ToolTipText = "Ouvrir l'image dans votre logiciel d'édition"
        '
        'TsBtnListSaveAs
        '
        Me.TsBtnListSaveAs.Enabled = False
        Me.TsBtnListSaveAs.Image = CType(resources.GetObject("TsBtnListSaveAs.Image"), System.Drawing.Image)
        Me.TsBtnListSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListSaveAs.Name = "TsBtnListSaveAs"
        Me.TsBtnListSaveAs.Size = New System.Drawing.Size(103, 22)
        Me.TsBtnListSaveAs.Text = "Enregistrer ..."
        Me.TsBtnListSaveAs.ToolTipText = "Enregistrer le/les fichiers sur votre disque dur"
        '
        'TsBtnListUp
        '
        Me.TsBtnListUp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsBtnListUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnListUp.Enabled = False
        Me.TsBtnListUp.Image = CType(resources.GetObject("TsBtnListUp.Image"), System.Drawing.Image)
        Me.TsBtnListUp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListUp.Name = "TsBtnListUp"
        Me.TsBtnListUp.Size = New System.Drawing.Size(28, 22)
        Me.TsBtnListUp.ToolTipText = "Remonter l'image"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnListDown
        '
        Me.TsBtnListDown.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsBtnListDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnListDown.Enabled = False
        Me.TsBtnListDown.Image = CType(resources.GetObject("TsBtnListDown.Image"), System.Drawing.Image)
        Me.TsBtnListDown.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListDown.Name = "TsBtnListDown"
        Me.TsBtnListDown.Size = New System.Drawing.Size(28, 22)
        Me.TsBtnListDown.ToolTipText = "Descendre l'image"
        '
        'TsBtnListSaveAnimation
        '
        Me.TsBtnListSaveAnimation.Enabled = False
        Me.TsBtnListSaveAnimation.Image = CType(resources.GetObject("TsBtnListSaveAnimation.Image"), System.Drawing.Image)
        Me.TsBtnListSaveAnimation.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnListSaveAnimation.Name = "TsBtnListSaveAnimation"
        Me.TsBtnListSaveAnimation.Size = New System.Drawing.Size(129, 28)
        Me.TsBtnListSaveAnimation.Text = "Créer Gif animé ..."
        Me.TsBtnListSaveAnimation.ToolTipText = "Créer et enregistrer une animation Gif depuis les fichiers sélectionnés dans la l" &
    "iste."
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.LvList)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GbxPreview)
        Me.SplitContainer2.Size = New System.Drawing.Size(594, 415)
        Me.SplitContainer2.SplitterDistance = 93
        Me.SplitContainer2.TabIndex = 0
        '
        'ImageListList
        '
        Me.ImageListList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListList.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListList.TransparentColor = System.Drawing.Color.Transparent
        '
        'GbxPreview
        '
        Me.GbxPreview.Controls.Add(Me.PcbListPicture)
        Me.GbxPreview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GbxPreview.Location = New System.Drawing.Point(0, 0)
        Me.GbxPreview.Name = "GbxPreview"
        Me.GbxPreview.Size = New System.Drawing.Size(594, 318)
        Me.GbxPreview.TabIndex = 15
        Me.GbxPreview.TabStop = False
        Me.GbxPreview.Text = "Aperçu"
        '
        'PcbListPicture
        '
        Me.PcbListPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PcbListPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PcbListPicture.Cursor = System.Windows.Forms.Cursors.Default
        Me.PcbListPicture.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PcbListPicture.Location = New System.Drawing.Point(3, 16)
        Me.PcbListPicture.Name = "PcbListPicture"
        Me.PcbListPicture.Size = New System.Drawing.Size(588, 299)
        Me.PcbListPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PcbListPicture.TabIndex = 9
        Me.PcbListPicture.TabStop = False
        '
        'BgwAddFiles
        '
        Me.BgwAddFiles.WorkerReportsProgress = True
        '
        'BgwGifAnimationStart
        '
        Me.BgwGifAnimationStart.WorkerReportsProgress = True
        Me.BgwGifAnimationStart.WorkerSupportsCancellation = True
        '
        'LvList
        '
        Me.LvList.AllowDrop = True
        Me.LvList.BackgroundImageTiled = True
        Me.LvList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhListName, Me.ClhListPath, Me.ClhListSize})
        Me.LvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LvList.FullRowSelect = True
        Me.LvList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LvList.HideSelection = False
        Me.LvList.LargeImageList = Me.ImageListList
        Me.LvList.Location = New System.Drawing.Point(0, 0)
        Me.LvList.Name = "LvList"
        Me.LvList.ShowItemToolTips = True
        Me.LvList.Size = New System.Drawing.Size(594, 93)
        Me.LvList.SmallImageList = Me.ImageListList
        Me.LvList.TabIndex = 7
        Me.LvList.UseCompatibleStateImageBehavior = False
        Me.LvList.View = System.Windows.Forms.View.Details
        '
        'ClhListName
        '
        Me.ClhListName.Text = "Nom"
        Me.ClhListName.Width = 154
        '
        'ClhListPath
        '
        Me.ClhListPath.Text = "Chemin"
        Me.ClhListPath.Width = 330
        '
        'ClhListSize
        '
        Me.ClhListSize.Text = "Taille"
        Me.ClhListSize.Width = 81
        '
        'UcList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "UcList"
        Me.Size = New System.Drawing.Size(594, 444)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TsList.ResumeLayout(False)
        Me.TsList.PerformLayout()
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.GbxPreview.ResumeLayout(False)
        Me.GbxPreview.PerformLayout()
        CType(Me.PcbListPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SplitContainer1 As Windows.Forms.SplitContainer
    Friend WithEvents TsList As Windows.Forms.ToolStrip
    Friend WithEvents TsBtnListCapture As Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As Windows.Forms.ToolStripSeparator
    Friend WithEvents TsBtnListAddFile As Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnListRemoveFile As Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As Windows.Forms.ToolStripSeparator
    Friend WithEvents TsBtnListEdit As Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnListSaveAs As Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnListUp As Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As Windows.Forms.ToolStripSeparator
    Friend WithEvents TsBtnListDown As Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnListSaveAnimation As Windows.Forms.ToolStripButton
    Friend WithEvents SplitContainer2 As Windows.Forms.SplitContainer
    Friend WithEvents LvList As ListviewEx
    Friend WithEvents ClhListName As Windows.Forms.ColumnHeader
    Friend WithEvents ClhListPath As Windows.Forms.ColumnHeader
    Friend WithEvents ClhListSize As Windows.Forms.ColumnHeader
    Friend WithEvents GbxPreview As Windows.Forms.GroupBox
    Friend WithEvents PcbListPicture As Windows.Forms.PictureBox
    Friend WithEvents BgwAddFiles As ComponentModel.BackgroundWorker
    Friend WithEvents ImageListList As Windows.Forms.ImageList
    Friend WithEvents BgwGifAnimationStart As ComponentModel.BackgroundWorker
End Class
