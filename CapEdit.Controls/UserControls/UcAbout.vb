﻿Imports System.Windows.Forms

Public Class UcAbout

#Region "Constructor "
    Public Sub New()
        InitializeComponent()
        Dock = DockStyle.Fill
        LblAboutInfosVersionString.Text = My.Application.Info.Version.ToString
    End Sub
#End Region

#Region " Methods "
    Private Sub LnkLblContact_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LnkLblContact.LinkClicked
        Process.Start("mailto:" & LnkLblContact.Text)
    End Sub

    Private Sub PictureBoxes_Click(sender As Object, e As EventArgs) Handles PcbIconHandler.Click
        Try
            Process.Start(sender.Tag.ToString)
        Catch ex As Exception
        End Try
    End Sub
#End Region

End Class
