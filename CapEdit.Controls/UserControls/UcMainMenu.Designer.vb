﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcMainMenu
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UcMainMenu))
        Me.TsMain = New System.Windows.Forms.ToolStrip()
        Me.TsBtnAbout = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnOptions = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnList = New System.Windows.Forms.ToolStripButton()
        Me.TsMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'TsMain
        '
        Me.TsMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsMain.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.TsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnAbout, Me.ToolStripSeparator9, Me.TsBtnOptions, Me.TsBtnList})
        Me.TsMain.Location = New System.Drawing.Point(0, 0)
        Me.TsMain.Name = "TsMain"
        Me.TsMain.Padding = New System.Windows.Forms.Padding(0, 0, 2, 0)
        Me.TsMain.Size = New System.Drawing.Size(602, 25)
        Me.TsMain.TabIndex = 1
        '
        'TsBtnAbout
        '
        Me.TsBtnAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsBtnAbout.Image = CType(resources.GetObject("TsBtnAbout.Image"), System.Drawing.Image)
        Me.TsBtnAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TsBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnAbout.Name = "TsBtnAbout"
        Me.TsBtnAbout.Size = New System.Drawing.Size(83, 22)
        Me.TsBtnAbout.Tag = "2"
        Me.TsBtnAbout.Text = "À propos"
        Me.TsBtnAbout.ToolTipText = "Informations, support et remerciements"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnOptions
        '
        Me.TsBtnOptions.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.TsBtnOptions.Image = CType(resources.GetObject("TsBtnOptions.Image"), System.Drawing.Image)
        Me.TsBtnOptions.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TsBtnOptions.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnOptions.Name = "TsBtnOptions"
        Me.TsBtnOptions.Size = New System.Drawing.Size(77, 22)
        Me.TsBtnOptions.Tag = "1"
        Me.TsBtnOptions.Text = "Options"
        Me.TsBtnOptions.ToolTipText = "Ajuster les paramètres de l'application"
        '
        'TsBtnList
        '
        Me.TsBtnList.Checked = True
        Me.TsBtnList.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TsBtnList.Image = CType(resources.GetObject("TsBtnList.Image"), System.Drawing.Image)
        Me.TsBtnList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnList.Name = "TsBtnList"
        Me.TsBtnList.Size = New System.Drawing.Size(165, 22)
        Me.TsBtnList.Tag = "0"
        Me.TsBtnList.Text = "File d'attente des images"
        Me.TsBtnList.ToolTipText = "Afficher la file d'attente des images capturées"
        '
        'UcMainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TsMain)
        Me.Name = "UcMainMenu"
        Me.Size = New System.Drawing.Size(602, 25)
        Me.TsMain.ResumeLayout(False)
        Me.TsMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TsMain As Windows.Forms.ToolStrip
    Friend WithEvents TsBtnAbout As Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator9 As Windows.Forms.ToolStripSeparator
    Friend WithEvents TsBtnOptions As Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnList As Windows.Forms.ToolStripButton
End Class
