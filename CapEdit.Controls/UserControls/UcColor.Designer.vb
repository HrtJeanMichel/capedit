﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcColor
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnWhite = New System.Windows.Forms.Button()
        Me.BtnBlack = New System.Windows.Forms.Button()
        Me.BtnBlue = New System.Windows.Forms.Button()
        Me.BtnGreen = New System.Windows.Forms.Button()
        Me.BtnYellow = New System.Windows.Forms.Button()
        Me.BtnOrange = New System.Windows.Forms.Button()
        Me.BtnRed = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnWhite
        '
        Me.BtnWhite.BackColor = System.Drawing.Color.White
        Me.BtnWhite.FlatAppearance.BorderSize = 0
        Me.BtnWhite.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnWhite.Location = New System.Drawing.Point(145, 1)
        Me.BtnWhite.Name = "BtnWhite"
        Me.BtnWhite.Size = New System.Drawing.Size(23, 23)
        Me.BtnWhite.TabIndex = 14
        Me.BtnWhite.UseVisualStyleBackColor = False
        '
        'BtnBlack
        '
        Me.BtnBlack.BackColor = System.Drawing.Color.Black
        Me.BtnBlack.FlatAppearance.BorderSize = 0
        Me.BtnBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBlack.Location = New System.Drawing.Point(121, 1)
        Me.BtnBlack.Name = "BtnBlack"
        Me.BtnBlack.Size = New System.Drawing.Size(23, 23)
        Me.BtnBlack.TabIndex = 13
        Me.BtnBlack.UseVisualStyleBackColor = False
        '
        'BtnBlue
        '
        Me.BtnBlue.BackColor = System.Drawing.Color.Blue
        Me.BtnBlue.FlatAppearance.BorderSize = 0
        Me.BtnBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBlue.Location = New System.Drawing.Point(97, 1)
        Me.BtnBlue.Name = "BtnBlue"
        Me.BtnBlue.Size = New System.Drawing.Size(23, 23)
        Me.BtnBlue.TabIndex = 12
        Me.BtnBlue.UseVisualStyleBackColor = False
        '
        'BtnGreen
        '
        Me.BtnGreen.BackColor = System.Drawing.Color.Green
        Me.BtnGreen.FlatAppearance.BorderSize = 0
        Me.BtnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnGreen.Location = New System.Drawing.Point(73, 1)
        Me.BtnGreen.Name = "BtnGreen"
        Me.BtnGreen.Size = New System.Drawing.Size(23, 23)
        Me.BtnGreen.TabIndex = 11
        Me.BtnGreen.UseVisualStyleBackColor = False
        '
        'BtnYellow
        '
        Me.BtnYellow.BackColor = System.Drawing.Color.Yellow
        Me.BtnYellow.FlatAppearance.BorderSize = 0
        Me.BtnYellow.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnYellow.Location = New System.Drawing.Point(49, 1)
        Me.BtnYellow.Name = "BtnYellow"
        Me.BtnYellow.Size = New System.Drawing.Size(23, 23)
        Me.BtnYellow.TabIndex = 10
        Me.BtnYellow.UseVisualStyleBackColor = False
        '
        'BtnOrange
        '
        Me.BtnOrange.BackColor = System.Drawing.Color.Orange
        Me.BtnOrange.FlatAppearance.BorderSize = 0
        Me.BtnOrange.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnOrange.Location = New System.Drawing.Point(25, 1)
        Me.BtnOrange.Name = "BtnOrange"
        Me.BtnOrange.Size = New System.Drawing.Size(23, 23)
        Me.BtnOrange.TabIndex = 9
        Me.BtnOrange.UseVisualStyleBackColor = False
        '
        'BtnRed
        '
        Me.BtnRed.BackColor = System.Drawing.Color.Red
        Me.BtnRed.FlatAppearance.BorderSize = 0
        Me.BtnRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRed.Location = New System.Drawing.Point(1, 1)
        Me.BtnRed.Name = "BtnRed"
        Me.BtnRed.Size = New System.Drawing.Size(23, 23)
        Me.BtnRed.TabIndex = 8
        Me.BtnRed.UseVisualStyleBackColor = False
        '
        'UcColor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.BtnWhite)
        Me.Controls.Add(Me.BtnBlack)
        Me.Controls.Add(Me.BtnBlue)
        Me.Controls.Add(Me.BtnGreen)
        Me.Controls.Add(Me.BtnYellow)
        Me.Controls.Add(Me.BtnOrange)
        Me.Controls.Add(Me.BtnRed)
        Me.Name = "UcColor"
        Me.Size = New System.Drawing.Size(169, 25)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BtnWhite As Windows.Forms.Button
    Friend WithEvents BtnBlack As Windows.Forms.Button
    Friend WithEvents BtnBlue As Windows.Forms.Button
    Friend WithEvents BtnGreen As Windows.Forms.Button
    Friend WithEvents BtnYellow As Windows.Forms.Button
    Friend WithEvents BtnOrange As Windows.Forms.Button
    Friend WithEvents BtnRed As Windows.Forms.Button
End Class
