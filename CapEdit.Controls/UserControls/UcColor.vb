﻿Imports System.Drawing
Imports System.Windows.Forms

Public Class UcColor

#Region " Fields "
    Private m_SelectedColor As Color
#End Region

#Region " Events "
    Public Event OnColorChanged(sender As Object, col As Color)
#End Region

#Region " Properties "
    Public Property SelectedColor() As Color
        Get
            Return m_SelectedColor
        End Get
        Set(ByVal value As Color)
            m_SelectedColor = value
            ButtonCheckedState(m_SelectedColor)
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        BtnRed.Tag = "-65536"
        BtnOrange.Tag = "-23296"
        BtnYellow.Tag = "-256"
        BtnGreen.Tag = "-16744448"
        BtnBlue.Tag = "-16776961"
        BtnBlack.Tag = "-16777216"
        BtnWhite.Tag = "-1"
    End Sub
#End Region

#Region " Methods "
    Public Sub Initialize(col As String)
        For Each Ctrl In Controls.Cast(Of Button)
            If Ctrl.Tag = col Then
                Ctrl.FlatAppearance.BorderSize = 1
                m_SelectedColor = Ctrl.BackColor
            Else
                Ctrl.FlatAppearance.BorderSize = 0
            End If
            SetForeColor(Ctrl)
        Next
    End Sub

    Private Sub SetForeColor(btn As Button)
        If m_SelectedColor = Color.White Then
            btn.ForeColor = Color.Black
        Else
            btn.ForeColor = Color.White
        End If
    End Sub

    Private Sub Buttons_Click(sender As Object, e As EventArgs) Handles BtnRed.Click, BtnOrange.Click, BtnYellow.Click, BtnGreen.Click, BtnBlue.Click, BtnBlack.Click, BtnWhite.Click
        Dim btn As Button = TryCast(sender, Button)
        ButtonCheckedState(btn.BackColor)
        RaiseEvent OnColorChanged(sender, btn.BackColor)
    End Sub

    Private Sub ButtonCheckedState(col As Color)
        For Each Ctrl In Controls.Cast(Of Button)
            If Ctrl.BackColor = col Then
                Ctrl.FlatAppearance.BorderSize = 1
                m_SelectedColor = col
            Else
                Ctrl.FlatAppearance.BorderSize = 0
            End If
            SetForeColor(Ctrl)
        Next
    End Sub
#End Region

End Class
