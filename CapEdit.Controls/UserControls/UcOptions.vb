﻿Imports System.Drawing
Imports System.Windows.Forms
Imports CapEdit.Helpers.Hook
Imports CapEdit.Helpers.Pictures
Imports CapEdit.Settings

Public Class UcOptions

#Region " Fields "
    Private m_OptionsExtensionClic As Boolean
    Private m_hKeyBoard As KeyBoard
    Private ReadOnly m_Settings As Parameters
    Private ReadOnly m_frmHandle As IntPtr
#End Region

#Region " Constructor "
    Public Sub New(frmHandle As IntPtr, Settings As Parameters)
        m_frmHandle = frmHandle
        m_Settings = Settings
        InitializeComponent()
        Dock = DockStyle.Fill

        If Settings.Options.CaptureExtension = Extension.jpgExt Then
            TxbOptionsJpgQuality.Text = Settings.Options.JpegCompression.ToString
            PnlOptionsJpgQuality.Visible = If(Settings.Options.CaptureExtension = Extension.jpgExt, True, False)
            TkbOptionsJpgQuality.Value = Settings.Options.JpegCompression
        End If

        TxbOptionsGifDelay.Text = Settings.Options.GifDelay.ToString
        TkbOptionsGifDelay.Value = Settings.Options.GifDelay
        ChbOptionsGifBorder.Checked = Settings.Options.GifBorder

        m_hKeyBoard = New KeyBoard(frmHandle, Settings.Options.HotKey1, Settings.Options.HotKey2)
        m_hKeyBoard.RegisterKey()

        CbxOptionsExtension.SelectedItem = Settings.Options.CaptureExtension
        CbxOptionsShortcutKey1.SelectedItem = KeyBoard.GetShortCutKeyName1(Settings.Options.HotKey1)
        CbxOptionsShortcutKey2.SelectedItem = KeyBoard.GetShortCutKeyName2(Settings.Options.HotKey2)
        ChbOptionsActiveWindow.Checked = Settings.Options.ActiveWindow
        ChbOptionsAfterCap.Checked = Settings.Options.AfterCapture

        ChbOptionsEditSoftDefault.Checked = Settings.Options.EditorDefault
        TxbOptionsEditSoft.Text = Settings.Options.EditApp
        PcbOptionsEditSoft.Image = Icon.ExtractAssociatedIcon(TxbOptionsEditSoft.Text).ToBitmap
    End Sub
#End Region

#Region " Methods "
    Private Sub CbxOptionsExtension_Click(sender As Object, e As EventArgs) Handles CbxOptionsExtension.Click
        m_OptionsExtensionClic = True
    End Sub

    Private Sub CbxOptionsExtension_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxOptionsExtension.SelectedIndexChanged
        If m_OptionsExtensionClic Then
            If CbxOptionsExtension.SelectedItem = Extension.jpgExt Then
                m_Settings.Options.CaptureExtension = CbxOptionsExtension.SelectedItem
                TxbOptionsJpgQuality.Text = m_Settings.Options.JpegCompression.ToString
                TkbOptionsJpgQuality.Value = m_Settings.Options.JpegCompression
                PnlOptionsJpgQuality.Visible = True
            Else
                m_Settings.Options.CaptureExtension = CbxOptionsExtension.SelectedItem
                PnlOptionsJpgQuality.Visible = False
            End If
            m_Settings.SaveFile()
            m_OptionsExtensionClic = False
        End If
    End Sub

    Private Sub CbxOptionsShortcutKey1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxOptionsShortcutKey1.SelectedIndexChanged
        m_Settings.Options.HotKey1 = KeyBoard.GetShortCutKey1(CbxOptionsShortcutKey1.SelectedItem)
        m_Settings.SaveFile()
        RegisterHotKey()
    End Sub

    Private Sub CbxOptionsShortcutKey2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxOptionsShortcutKey2.SelectedIndexChanged
        m_Settings.Options.HotKey2 = KeyBoard.GetShortCutKey2(CbxOptionsShortcutKey2.SelectedItem)
        m_Settings.SaveFile()
        RegisterHotKey()
    End Sub

    Private Sub RegisterHotKey()
        m_hKeyBoard.UnregisterKey()
        m_hKeyBoard = New KeyBoard(m_frmHandle, m_Settings.Options.HotKey1, m_Settings.Options.HotKey2)
        m_hKeyBoard.RegisterKey()
    End Sub

    Private Sub ChbOptionsActiveWindow_CheckedChanged(sender As Object, e As EventArgs) Handles ChbOptionsActiveWindow.CheckedChanged
        m_Settings.Options.ActiveWindow = ChbOptionsActiveWindow.Checked
        m_Settings.SaveFile()
    End Sub

    Private Sub ChbOptionsAfterCap_CheckedChanged(sender As Object, e As EventArgs) Handles ChbOptionsAfterCap.CheckedChanged
        m_Settings.Options.AfterCapture = ChbOptionsAfterCap.Checked
        m_Settings.SaveFile()
    End Sub

    Private Sub BtnOptionsEditSoft_Click(sender As Object, e As EventArgs) Handles BtnOptionsEditSoft.Click
        Using ofd As New OpenFileDialog
            With ofd
                .Filter = "Programme|*.exe;*.exe"
                .Title = "Sélectionnez un programme d'édition d'image"
                .CheckFileExists = True
                .Multiselect = False
                If .ShowDialog() = DialogResult.OK Then
                    m_Settings.Options.EditApp = .FileName
                    m_Settings.SaveFile()
                    TxbOptionsEditSoft.Text = .FileName
                    PcbOptionsEditSoft.Image = Icon.ExtractAssociatedIcon(TxbOptionsEditSoft.Text).ToBitmap
                End If
            End With
        End Using
    End Sub

    Private Sub ChbOptionsEditSoftDefault_CheckedChanged(sender As Object, e As EventArgs) Handles ChbOptionsEditSoftDefault.CheckedChanged
        PcbOptionsEditSoft.Enabled = Not ChbOptionsEditSoftDefault.Checked
        TxbOptionsEditSoft.Enabled = Not ChbOptionsEditSoftDefault.Checked
        BtnOptionsEditSoft.Enabled = Not ChbOptionsEditSoftDefault.Checked

        m_Settings.Options.EditorDefault = ChbOptionsEditSoftDefault.Checked
        m_Settings.SaveFile()
    End Sub

    Private Sub TkbOptionsJpgQuality_ValueChanged(sender As Object, e As EventArgs) Handles TkbOptionsJpgQuality.ValueChanged
        If CbxOptionsExtension.SelectedItem = Extension.jpgExt Then
            TxbOptionsJpgQuality.Text = TkbOptionsJpgQuality.Value.ToString
            m_Settings.Options.JpegCompression = TkbOptionsJpgQuality.Value
        End If
        m_Settings.SaveFile()
    End Sub

    Private Sub TkbOptionsGifDelay_ValueChanged(sender As Object, e As EventArgs) Handles TkbOptionsGifDelay.ValueChanged
        TxbOptionsGifDelay.Text = TkbOptionsGifDelay.Value.ToString
        m_Settings.Options.GifDelay = TkbOptionsGifDelay.Value
        m_Settings.SaveFile()
    End Sub

    Private Sub TkbOptionsGifDelay_Scroll(sender As Object, e As EventArgs) Handles TkbOptionsGifDelay.Scroll
        Dim value As Integer = (TryCast(sender, TrackBar)).Value
        Dim indexDbl As Double = (value * 1.0) / TkbOptionsGifDelay.TickFrequency
        Dim index As Integer = Convert.ToInt32(Math.Round(indexDbl))
        TkbOptionsGifDelay.Value = TkbOptionsGifDelay.TickFrequency * index
        TxbOptionsGifDelay.Text = TkbOptionsGifDelay.Value.ToString()
    End Sub

    Private Sub ChbOptionsGifBorder_CheckedChanged(sender As Object, e As EventArgs) Handles ChbOptionsGifBorder.CheckedChanged
        m_Settings.Options.GifBorder = ChbOptionsGifBorder.Checked
        m_Settings.SaveFile()
    End Sub
#End Region

End Class
