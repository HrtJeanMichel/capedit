﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcAbout
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UcAbout))
        Me.GbxAboutCredits = New System.Windows.Forms.GroupBox()
        Me.PcbIconHandler = New System.Windows.Forms.PictureBox()
        Me.LblAboutCreditsIconEx = New System.Windows.Forms.Label()
        Me.GbxAboutInfos = New System.Windows.Forms.GroupBox()
        Me.LnkLblContact = New System.Windows.Forms.LinkLabel()
        Me.LblAboutInfosSite = New System.Windows.Forms.Label()
        Me.LblAboutInfosVersion = New System.Windows.Forms.Label()
        Me.LblAboutInfosVersionString = New System.Windows.Forms.Label()
        Me.GbxAboutCredits.SuspendLayout()
        CType(Me.PcbIconHandler, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxAboutInfos.SuspendLayout()
        Me.SuspendLayout()
        '
        'GbxAboutCredits
        '
        Me.GbxAboutCredits.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxAboutCredits.Controls.Add(Me.PcbIconHandler)
        Me.GbxAboutCredits.Controls.Add(Me.LblAboutCreditsIconEx)
        Me.GbxAboutCredits.Location = New System.Drawing.Point(33, 124)
        Me.GbxAboutCredits.Name = "GbxAboutCredits"
        Me.GbxAboutCredits.Size = New System.Drawing.Size(523, 97)
        Me.GbxAboutCredits.TabIndex = 24
        Me.GbxAboutCredits.TabStop = False
        Me.GbxAboutCredits.Text = "Remerciements"
        '
        'PcbIconHandler
        '
        Me.PcbIconHandler.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PcbIconHandler.Image = CType(resources.GetObject("PcbIconHandler.Image"), System.Drawing.Image)
        Me.PcbIconHandler.Location = New System.Drawing.Point(248, 38)
        Me.PcbIconHandler.Name = "PcbIconHandler"
        Me.PcbIconHandler.Size = New System.Drawing.Size(20, 21)
        Me.PcbIconHandler.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbIconHandler.TabIndex = 14
        Me.PcbIconHandler.TabStop = False
        Me.PcbIconHandler.Tag = "https://stackoverflow.com/questions/616718/how-do-i-get-common-file-type-icons-in" &
    "-c/616829#616829"
        '
        'LblAboutCreditsIconEx
        '
        Me.LblAboutCreditsIconEx.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblAboutCreditsIconEx.AutoSize = True
        Me.LblAboutCreditsIconEx.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAboutCreditsIconEx.Location = New System.Drawing.Point(122, 40)
        Me.LblAboutCreditsIconEx.Name = "LblAboutCreditsIconEx"
        Me.LblAboutCreditsIconEx.Size = New System.Drawing.Size(71, 13)
        Me.LblAboutCreditsIconEx.TabIndex = 13
        Me.LblAboutCreditsIconEx.Text = "IconHandler :"
        '
        'GbxAboutInfos
        '
        Me.GbxAboutInfos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxAboutInfos.Controls.Add(Me.LnkLblContact)
        Me.GbxAboutInfos.Controls.Add(Me.LblAboutInfosSite)
        Me.GbxAboutInfos.Controls.Add(Me.LblAboutInfosVersion)
        Me.GbxAboutInfos.Controls.Add(Me.LblAboutInfosVersionString)
        Me.GbxAboutInfos.Location = New System.Drawing.Point(33, 25)
        Me.GbxAboutInfos.Name = "GbxAboutInfos"
        Me.GbxAboutInfos.Size = New System.Drawing.Size(523, 93)
        Me.GbxAboutInfos.TabIndex = 23
        Me.GbxAboutInfos.TabStop = False
        Me.GbxAboutInfos.Text = "Informations"
        '
        'LnkLblContact
        '
        Me.LnkLblContact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LnkLblContact.AutoSize = True
        Me.LnkLblContact.Location = New System.Drawing.Point(238, 57)
        Me.LnkLblContact.Name = "LnkLblContact"
        Me.LnkLblContact.Size = New System.Drawing.Size(147, 13)
        Me.LnkLblContact.TabIndex = 24
        Me.LnkLblContact.TabStop = True
        Me.LnkLblContact.Text = "heraultjeanmichel@gmail.com"
        '
        'LblAboutInfosSite
        '
        Me.LblAboutInfosSite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblAboutInfosSite.AutoSize = True
        Me.LblAboutInfosSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAboutInfosSite.Location = New System.Drawing.Point(143, 57)
        Me.LblAboutInfosSite.Name = "LblAboutInfosSite"
        Me.LblAboutInfosSite.Size = New System.Drawing.Size(50, 13)
        Me.LblAboutInfosSite.TabIndex = 20
        Me.LblAboutInfosSite.Text = "Contact :"
        '
        'LblAboutInfosVersion
        '
        Me.LblAboutInfosVersion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblAboutInfosVersion.AutoSize = True
        Me.LblAboutInfosVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAboutInfosVersion.Location = New System.Drawing.Point(145, 31)
        Me.LblAboutInfosVersion.Name = "LblAboutInfosVersion"
        Me.LblAboutInfosVersion.Size = New System.Drawing.Size(48, 13)
        Me.LblAboutInfosVersion.TabIndex = 17
        Me.LblAboutInfosVersion.Text = "Version :"
        '
        'LblAboutInfosVersionString
        '
        Me.LblAboutInfosVersionString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblAboutInfosVersionString.AutoSize = True
        Me.LblAboutInfosVersionString.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAboutInfosVersionString.Location = New System.Drawing.Point(238, 31)
        Me.LblAboutInfosVersionString.Name = "LblAboutInfosVersionString"
        Me.LblAboutInfosVersionString.Size = New System.Drawing.Size(40, 13)
        Me.LblAboutInfosVersionString.TabIndex = 14
        Me.LblAboutInfosVersionString.Text = "3.3.0.0"
        '
        'UcAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GbxAboutCredits)
        Me.Controls.Add(Me.GbxAboutInfos)
        Me.Name = "UcAbout"
        Me.Size = New System.Drawing.Size(588, 488)
        Me.GbxAboutCredits.ResumeLayout(False)
        Me.GbxAboutCredits.PerformLayout()
        CType(Me.PcbIconHandler, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxAboutInfos.ResumeLayout(False)
        Me.GbxAboutInfos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GbxAboutCredits As Windows.Forms.GroupBox
    Friend WithEvents GbxAboutInfos As Windows.Forms.GroupBox
    Friend WithEvents LnkLblContact As Windows.Forms.LinkLabel
    Friend WithEvents LblAboutInfosSite As Windows.Forms.Label
    Friend WithEvents LblAboutInfosVersion As Windows.Forms.Label
    Friend WithEvents LblAboutInfosVersionString As Windows.Forms.Label
    Friend WithEvents LblAboutCreditsIconEx As Windows.Forms.Label
    Friend WithEvents PcbIconHandler As Windows.Forms.PictureBox
End Class
