﻿Imports System.Windows.Forms

Public Class UcMainMenu

#Region " Events "
    Public Event OnMainMenuButtonClick(sender As Object, e As EventArgs)
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        Dock = DockStyle.Fill
    End Sub
#End Region

#Region " Methods "
    Private Sub TsBtnLists_Click(sender As Object, e As EventArgs) Handles TsBtnList.Click, TsBtnOptions.Click, TsBtnAbout.Click
        CheckUncheckButtons(TryCast(sender, ToolStripButton).Tag)
        RaiseEvent OnMainMenuButtonClick(sender, e)
    End Sub

    Private Sub CheckUncheckButtons(i As Integer)
        For Each c As ToolStripItem In TsMain.Items
            If TypeOf TryCast(c, ToolStripButton) Is ToolStripButton Then
                Dim tsb As ToolStripButton = TryCast(c, ToolStripButton)
                If Not tsb.Tag Is Nothing Then
                    tsb.Checked = If(CInt(tsb.Tag) = i, True, False)
                End If
            End If
        Next
    End Sub
#End Region

End Class
