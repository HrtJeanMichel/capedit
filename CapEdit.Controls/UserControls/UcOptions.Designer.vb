﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UcOptions
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GbxOptionsGif = New System.Windows.Forms.GroupBox()
        Me.TxbOptionsGifDelay = New System.Windows.Forms.TextBox()
        Me.LblOptionsGifDelay = New System.Windows.Forms.Label()
        Me.TkbOptionsGifDelay = New System.Windows.Forms.TrackBar()
        Me.ChbOptionsGifBorder = New System.Windows.Forms.CheckBox()
        Me.GbxOptionsEditSoft = New System.Windows.Forms.GroupBox()
        Me.ChbOptionsEditSoftDefault = New System.Windows.Forms.CheckBox()
        Me.PcbOptionsEditSoft = New System.Windows.Forms.PictureBox()
        Me.TxbOptionsEditSoft = New System.Windows.Forms.TextBox()
        Me.BtnOptionsEditSoft = New System.Windows.Forms.Button()
        Me.GbxOptionsAfterCap = New System.Windows.Forms.GroupBox()
        Me.ChbOptionsAfterCap = New System.Windows.Forms.CheckBox()
        Me.GbxOptionsShortcuts = New System.Windows.Forms.GroupBox()
        Me.ChbOptionsActiveWindow = New System.Windows.Forms.CheckBox()
        Me.CbxOptionsShortcutKey2 = New System.Windows.Forms.ComboBox()
        Me.LblOptionsShortcuts2 = New System.Windows.Forms.Label()
        Me.LblOptionsShortcuts = New System.Windows.Forms.Label()
        Me.CbxOptionsShortcutKey1 = New System.Windows.Forms.ComboBox()
        Me.GbxOptionsExtension = New System.Windows.Forms.GroupBox()
        Me.PnlOptionsJpgQuality = New System.Windows.Forms.Panel()
        Me.TxbOptionsJpgQuality = New System.Windows.Forms.TextBox()
        Me.LblOptionsJpgQuality = New System.Windows.Forms.Label()
        Me.TkbOptionsJpgQuality = New System.Windows.Forms.TrackBar()
        Me.LblOptionsExtension = New System.Windows.Forms.Label()
        Me.CbxOptionsExtension = New System.Windows.Forms.ComboBox()
        Me.GbxOptionsGif.SuspendLayout()
        CType(Me.TkbOptionsGifDelay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxOptionsEditSoft.SuspendLayout()
        CType(Me.PcbOptionsEditSoft, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxOptionsAfterCap.SuspendLayout()
        Me.GbxOptionsShortcuts.SuspendLayout()
        Me.GbxOptionsExtension.SuspendLayout()
        Me.PnlOptionsJpgQuality.SuspendLayout()
        CType(Me.TkbOptionsJpgQuality, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GbxOptionsGif
        '
        Me.GbxOptionsGif.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxOptionsGif.Controls.Add(Me.TxbOptionsGifDelay)
        Me.GbxOptionsGif.Controls.Add(Me.LblOptionsGifDelay)
        Me.GbxOptionsGif.Controls.Add(Me.TkbOptionsGifDelay)
        Me.GbxOptionsGif.Controls.Add(Me.ChbOptionsGifBorder)
        Me.GbxOptionsGif.Location = New System.Drawing.Point(33, 339)
        Me.GbxOptionsGif.Name = "GbxOptionsGif"
        Me.GbxOptionsGif.Size = New System.Drawing.Size(529, 97)
        Me.GbxOptionsGif.TabIndex = 18
        Me.GbxOptionsGif.TabStop = False
        Me.GbxOptionsGif.Text = "Paramètres de l'animation GIF"
        '
        'TxbOptionsGifDelay
        '
        Me.TxbOptionsGifDelay.Location = New System.Drawing.Point(464, 33)
        Me.TxbOptionsGifDelay.Name = "TxbOptionsGifDelay"
        Me.TxbOptionsGifDelay.ReadOnly = True
        Me.TxbOptionsGifDelay.Size = New System.Drawing.Size(40, 20)
        Me.TxbOptionsGifDelay.TabIndex = 14
        '
        'LblOptionsGifDelay
        '
        Me.LblOptionsGifDelay.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblOptionsGifDelay.AutoSize = True
        Me.LblOptionsGifDelay.Location = New System.Drawing.Point(54, 33)
        Me.LblOptionsGifDelay.Name = "LblOptionsGifDelay"
        Me.LblOptionsGifDelay.Size = New System.Drawing.Size(79, 13)
        Me.LblOptionsGifDelay.TabIndex = 13
        Me.LblOptionsGifDelay.Text = "Temporisation :"
        '
        'TkbOptionsGifDelay
        '
        Me.TkbOptionsGifDelay.BackColor = System.Drawing.SystemColors.Control
        Me.TkbOptionsGifDelay.LargeChange = 400
        Me.TkbOptionsGifDelay.Location = New System.Drawing.Point(139, 19)
        Me.TkbOptionsGifDelay.Maximum = 3200
        Me.TkbOptionsGifDelay.Name = "TkbOptionsGifDelay"
        Me.TkbOptionsGifDelay.Size = New System.Drawing.Size(319, 45)
        Me.TkbOptionsGifDelay.SmallChange = 400
        Me.TkbOptionsGifDelay.TabIndex = 12
        Me.TkbOptionsGifDelay.TickFrequency = 400
        Me.TkbOptionsGifDelay.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        '
        'ChbOptionsGifBorder
        '
        Me.ChbOptionsGifBorder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ChbOptionsGifBorder.AutoSize = True
        Me.ChbOptionsGifBorder.Location = New System.Drawing.Point(146, 66)
        Me.ChbOptionsGifBorder.Name = "ChbOptionsGifBorder"
        Me.ChbOptionsGifBorder.Size = New System.Drawing.Size(276, 17)
        Me.ChbOptionsGifBorder.TabIndex = 0
        Me.ChbOptionsGifBorder.Text = "Dessiner un contour noir de 1 pixel sur chaque image"
        Me.ChbOptionsGifBorder.UseVisualStyleBackColor = True
        '
        'GbxOptionsEditSoft
        '
        Me.GbxOptionsEditSoft.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxOptionsEditSoft.Controls.Add(Me.ChbOptionsEditSoftDefault)
        Me.GbxOptionsEditSoft.Controls.Add(Me.PcbOptionsEditSoft)
        Me.GbxOptionsEditSoft.Controls.Add(Me.TxbOptionsEditSoft)
        Me.GbxOptionsEditSoft.Controls.Add(Me.BtnOptionsEditSoft)
        Me.GbxOptionsEditSoft.Location = New System.Drawing.Point(33, 239)
        Me.GbxOptionsEditSoft.Name = "GbxOptionsEditSoft"
        Me.GbxOptionsEditSoft.Size = New System.Drawing.Size(529, 95)
        Me.GbxOptionsEditSoft.TabIndex = 17
        Me.GbxOptionsEditSoft.TabStop = False
        Me.GbxOptionsEditSoft.Text = "Sélection du logiciel d'édition"
        '
        'ChbOptionsEditSoftDefault
        '
        Me.ChbOptionsEditSoftDefault.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ChbOptionsEditSoftDefault.AutoSize = True
        Me.ChbOptionsEditSoftDefault.Location = New System.Drawing.Point(146, 28)
        Me.ChbOptionsEditSoftDefault.Name = "ChbOptionsEditSoftDefault"
        Me.ChbOptionsEditSoftDefault.Size = New System.Drawing.Size(245, 17)
        Me.ChbOptionsEditSoftDefault.TabIndex = 3
        Me.ChbOptionsEditSoftDefault.Text = "Editer les images avec l'éditeur simplifié intégré"
        Me.ChbOptionsEditSoftDefault.UseVisualStyleBackColor = True
        '
        'PcbOptionsEditSoft
        '
        Me.PcbOptionsEditSoft.Location = New System.Drawing.Point(23, 52)
        Me.PcbOptionsEditSoft.Name = "PcbOptionsEditSoft"
        Me.PcbOptionsEditSoft.Size = New System.Drawing.Size(30, 31)
        Me.PcbOptionsEditSoft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PcbOptionsEditSoft.TabIndex = 2
        Me.PcbOptionsEditSoft.TabStop = False
        '
        'TxbOptionsEditSoft
        '
        Me.TxbOptionsEditSoft.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxbOptionsEditSoft.Location = New System.Drawing.Point(69, 59)
        Me.TxbOptionsEditSoft.Name = "TxbOptionsEditSoft"
        Me.TxbOptionsEditSoft.ReadOnly = True
        Me.TxbOptionsEditSoft.Size = New System.Drawing.Size(395, 20)
        Me.TxbOptionsEditSoft.TabIndex = 1
        '
        'BtnOptionsEditSoft
        '
        Me.BtnOptionsEditSoft.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnOptionsEditSoft.Location = New System.Drawing.Point(470, 57)
        Me.BtnOptionsEditSoft.Name = "BtnOptionsEditSoft"
        Me.BtnOptionsEditSoft.Size = New System.Drawing.Size(40, 23)
        Me.BtnOptionsEditSoft.TabIndex = 0
        Me.BtnOptionsEditSoft.Text = "..."
        Me.BtnOptionsEditSoft.UseVisualStyleBackColor = True
        '
        'GbxOptionsAfterCap
        '
        Me.GbxOptionsAfterCap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxOptionsAfterCap.Controls.Add(Me.ChbOptionsAfterCap)
        Me.GbxOptionsAfterCap.Location = New System.Drawing.Point(33, 162)
        Me.GbxOptionsAfterCap.Name = "GbxOptionsAfterCap"
        Me.GbxOptionsAfterCap.Size = New System.Drawing.Size(529, 71)
        Me.GbxOptionsAfterCap.TabIndex = 16
        Me.GbxOptionsAfterCap.TabStop = False
        Me.GbxOptionsAfterCap.Text = "Action après la capture"
        '
        'ChbOptionsAfterCap
        '
        Me.ChbOptionsAfterCap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ChbOptionsAfterCap.AutoSize = True
        Me.ChbOptionsAfterCap.Location = New System.Drawing.Point(146, 31)
        Me.ChbOptionsAfterCap.Name = "ChbOptionsAfterCap"
        Me.ChbOptionsAfterCap.Size = New System.Drawing.Size(237, 17)
        Me.ChbOptionsAfterCap.TabIndex = 0
        Me.ChbOptionsAfterCap.Text = "Réduire la fenêtre principale après la capture"
        Me.ChbOptionsAfterCap.UseVisualStyleBackColor = True
        '
        'GbxOptionsShortcuts
        '
        Me.GbxOptionsShortcuts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxOptionsShortcuts.Controls.Add(Me.ChbOptionsActiveWindow)
        Me.GbxOptionsShortcuts.Controls.Add(Me.CbxOptionsShortcutKey2)
        Me.GbxOptionsShortcuts.Controls.Add(Me.LblOptionsShortcuts2)
        Me.GbxOptionsShortcuts.Controls.Add(Me.LblOptionsShortcuts)
        Me.GbxOptionsShortcuts.Controls.Add(Me.CbxOptionsShortcutKey1)
        Me.GbxOptionsShortcuts.Location = New System.Drawing.Point(33, 87)
        Me.GbxOptionsShortcuts.Name = "GbxOptionsShortcuts"
        Me.GbxOptionsShortcuts.Size = New System.Drawing.Size(529, 69)
        Me.GbxOptionsShortcuts.TabIndex = 15
        Me.GbxOptionsShortcuts.TabStop = False
        Me.GbxOptionsShortcuts.Text = "Raccourcis clavier pour la capture lorsque CapEdit est réduit en zone de notifica" &
    "tions"
        '
        'ChbOptionsActiveWindow
        '
        Me.ChbOptionsActiveWindow.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ChbOptionsActiveWindow.AutoSize = True
        Me.ChbOptionsActiveWindow.Location = New System.Drawing.Point(346, 32)
        Me.ChbOptionsActiveWindow.Name = "ChbOptionsActiveWindow"
        Me.ChbOptionsActiveWindow.Size = New System.Drawing.Size(94, 17)
        Me.ChbOptionsActiveWindow.TabIndex = 7
        Me.ChbOptionsActiveWindow.Text = "Fenêtre active"
        Me.ChbOptionsActiveWindow.UseVisualStyleBackColor = True
        '
        'CbxOptionsShortcutKey2
        '
        Me.CbxOptionsShortcutKey2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CbxOptionsShortcutKey2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxOptionsShortcutKey2.FormattingEnabled = True
        Me.CbxOptionsShortcutKey2.Items.AddRange(New Object() {"Impecr", "Insert", "Suppr", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"})
        Me.CbxOptionsShortcutKey2.Location = New System.Drawing.Point(254, 30)
        Me.CbxOptionsShortcutKey2.Name = "CbxOptionsShortcutKey2"
        Me.CbxOptionsShortcutKey2.Size = New System.Drawing.Size(64, 21)
        Me.CbxOptionsShortcutKey2.TabIndex = 6
        '
        'LblOptionsShortcuts2
        '
        Me.LblOptionsShortcuts2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblOptionsShortcuts2.AutoSize = True
        Me.LblOptionsShortcuts2.Location = New System.Drawing.Point(236, 33)
        Me.LblOptionsShortcuts2.Name = "LblOptionsShortcuts2"
        Me.LblOptionsShortcuts2.Size = New System.Drawing.Size(13, 13)
        Me.LblOptionsShortcuts2.TabIndex = 5
        Me.LblOptionsShortcuts2.Text = "+"
        '
        'LblOptionsShortcuts
        '
        Me.LblOptionsShortcuts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblOptionsShortcuts.AutoSize = True
        Me.LblOptionsShortcuts.Location = New System.Drawing.Point(87, 33)
        Me.LblOptionsShortcuts.Name = "LblOptionsShortcuts"
        Me.LblOptionsShortcuts.Size = New System.Drawing.Size(73, 13)
        Me.LblOptionsShortcuts.TabIndex = 4
        Me.LblOptionsShortcuts.Text = "Combinaison :"
        '
        'CbxOptionsShortcutKey1
        '
        Me.CbxOptionsShortcutKey1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CbxOptionsShortcutKey1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxOptionsShortcutKey1.FormattingEnabled = True
        Me.CbxOptionsShortcutKey1.Items.AddRange(New Object() {"Alt", "Ctrl", "Shift", "Win"})
        Me.CbxOptionsShortcutKey1.Location = New System.Drawing.Point(166, 30)
        Me.CbxOptionsShortcutKey1.Name = "CbxOptionsShortcutKey1"
        Me.CbxOptionsShortcutKey1.Size = New System.Drawing.Size(64, 21)
        Me.CbxOptionsShortcutKey1.TabIndex = 3
        '
        'GbxOptionsExtension
        '
        Me.GbxOptionsExtension.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GbxOptionsExtension.Controls.Add(Me.PnlOptionsJpgQuality)
        Me.GbxOptionsExtension.Controls.Add(Me.LblOptionsExtension)
        Me.GbxOptionsExtension.Controls.Add(Me.CbxOptionsExtension)
        Me.GbxOptionsExtension.Location = New System.Drawing.Point(33, 4)
        Me.GbxOptionsExtension.Name = "GbxOptionsExtension"
        Me.GbxOptionsExtension.Size = New System.Drawing.Size(529, 77)
        Me.GbxOptionsExtension.TabIndex = 14
        Me.GbxOptionsExtension.TabStop = False
        Me.GbxOptionsExtension.Text = "Extension de la capture"
        '
        'PnlOptionsJpgQuality
        '
        Me.PnlOptionsJpgQuality.Controls.Add(Me.TxbOptionsJpgQuality)
        Me.PnlOptionsJpgQuality.Controls.Add(Me.LblOptionsJpgQuality)
        Me.PnlOptionsJpgQuality.Controls.Add(Me.TkbOptionsJpgQuality)
        Me.PnlOptionsJpgQuality.Location = New System.Drawing.Point(246, 19)
        Me.PnlOptionsJpgQuality.Name = "PnlOptionsJpgQuality"
        Me.PnlOptionsJpgQuality.Size = New System.Drawing.Size(271, 55)
        Me.PnlOptionsJpgQuality.TabIndex = 9
        Me.PnlOptionsJpgQuality.Visible = False
        '
        'TxbOptionsJpgQuality
        '
        Me.TxbOptionsJpgQuality.Location = New System.Drawing.Point(218, 15)
        Me.TxbOptionsJpgQuality.Name = "TxbOptionsJpgQuality"
        Me.TxbOptionsJpgQuality.ReadOnly = True
        Me.TxbOptionsJpgQuality.Size = New System.Drawing.Size(40, 20)
        Me.TxbOptionsJpgQuality.TabIndex = 11
        '
        'LblOptionsJpgQuality
        '
        Me.LblOptionsJpgQuality.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblOptionsJpgQuality.AutoSize = True
        Me.LblOptionsJpgQuality.Location = New System.Drawing.Point(18, 17)
        Me.LblOptionsJpgQuality.Name = "LblOptionsJpgQuality"
        Me.LblOptionsJpgQuality.Size = New System.Drawing.Size(46, 13)
        Me.LblOptionsJpgQuality.TabIndex = 10
        Me.LblOptionsJpgQuality.Text = "Qualité :"
        '
        'TkbOptionsJpgQuality
        '
        Me.TkbOptionsJpgQuality.BackColor = System.Drawing.SystemColors.Control
        Me.TkbOptionsJpgQuality.Location = New System.Drawing.Point(70, 4)
        Me.TkbOptionsJpgQuality.Maximum = 100
        Me.TkbOptionsJpgQuality.Minimum = 10
        Me.TkbOptionsJpgQuality.Name = "TkbOptionsJpgQuality"
        Me.TkbOptionsJpgQuality.Size = New System.Drawing.Size(142, 45)
        Me.TkbOptionsJpgQuality.TabIndex = 9
        Me.TkbOptionsJpgQuality.TickFrequency = 5
        Me.TkbOptionsJpgQuality.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.TkbOptionsJpgQuality.Value = 10
        '
        'LblOptionsExtension
        '
        Me.LblOptionsExtension.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblOptionsExtension.AutoSize = True
        Me.LblOptionsExtension.Location = New System.Drawing.Point(101, 37)
        Me.LblOptionsExtension.Name = "LblOptionsExtension"
        Me.LblOptionsExtension.Size = New System.Drawing.Size(59, 13)
        Me.LblOptionsExtension.TabIndex = 4
        Me.LblOptionsExtension.Text = "Extension :"
        '
        'CbxOptionsExtension
        '
        Me.CbxOptionsExtension.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CbxOptionsExtension.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbxOptionsExtension.FormattingEnabled = True
        Me.CbxOptionsExtension.Items.AddRange(New Object() {".bmp", ".gif", ".jpg", ".png"})
        Me.CbxOptionsExtension.Location = New System.Drawing.Point(166, 34)
        Me.CbxOptionsExtension.Name = "CbxOptionsExtension"
        Me.CbxOptionsExtension.Size = New System.Drawing.Size(64, 21)
        Me.CbxOptionsExtension.TabIndex = 3
        '
        'UcOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GbxOptionsGif)
        Me.Controls.Add(Me.GbxOptionsEditSoft)
        Me.Controls.Add(Me.GbxOptionsAfterCap)
        Me.Controls.Add(Me.GbxOptionsShortcuts)
        Me.Controls.Add(Me.GbxOptionsExtension)
        Me.Name = "UcOptions"
        Me.Size = New System.Drawing.Size(594, 444)
        Me.GbxOptionsGif.ResumeLayout(False)
        Me.GbxOptionsGif.PerformLayout()
        CType(Me.TkbOptionsGifDelay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxOptionsEditSoft.ResumeLayout(False)
        Me.GbxOptionsEditSoft.PerformLayout()
        CType(Me.PcbOptionsEditSoft, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxOptionsAfterCap.ResumeLayout(False)
        Me.GbxOptionsAfterCap.PerformLayout()
        Me.GbxOptionsShortcuts.ResumeLayout(False)
        Me.GbxOptionsShortcuts.PerformLayout()
        Me.GbxOptionsExtension.ResumeLayout(False)
        Me.GbxOptionsExtension.PerformLayout()
        Me.PnlOptionsJpgQuality.ResumeLayout(False)
        Me.PnlOptionsJpgQuality.PerformLayout()
        CType(Me.TkbOptionsJpgQuality, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GbxOptionsGif As Windows.Forms.GroupBox
    Friend WithEvents TxbOptionsGifDelay As Windows.Forms.TextBox
    Friend WithEvents LblOptionsGifDelay As Windows.Forms.Label
    Friend WithEvents TkbOptionsGifDelay As Windows.Forms.TrackBar
    Friend WithEvents ChbOptionsGifBorder As Windows.Forms.CheckBox
    Friend WithEvents GbxOptionsEditSoft As Windows.Forms.GroupBox
    Friend WithEvents ChbOptionsEditSoftDefault As Windows.Forms.CheckBox
    Friend WithEvents PcbOptionsEditSoft As Windows.Forms.PictureBox
    Friend WithEvents TxbOptionsEditSoft As Windows.Forms.TextBox
    Friend WithEvents BtnOptionsEditSoft As Windows.Forms.Button
    Friend WithEvents GbxOptionsAfterCap As Windows.Forms.GroupBox
    Friend WithEvents ChbOptionsAfterCap As Windows.Forms.CheckBox
    Friend WithEvents GbxOptionsShortcuts As Windows.Forms.GroupBox
    Friend WithEvents ChbOptionsActiveWindow As Windows.Forms.CheckBox
    Friend WithEvents CbxOptionsShortcutKey2 As Windows.Forms.ComboBox
    Friend WithEvents LblOptionsShortcuts2 As Windows.Forms.Label
    Friend WithEvents LblOptionsShortcuts As Windows.Forms.Label
    Friend WithEvents CbxOptionsShortcutKey1 As Windows.Forms.ComboBox
    Friend WithEvents GbxOptionsExtension As Windows.Forms.GroupBox
    Friend WithEvents PnlOptionsJpgQuality As Windows.Forms.Panel
    Friend WithEvents TxbOptionsJpgQuality As Windows.Forms.TextBox
    Friend WithEvents LblOptionsJpgQuality As Windows.Forms.Label
    Friend WithEvents TkbOptionsJpgQuality As Windows.Forms.TrackBar
    Friend WithEvents LblOptionsExtension As Windows.Forms.Label
    Friend WithEvents CbxOptionsExtension As Windows.Forms.ComboBox
End Class
