﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.IO
Imports System.Threading
Imports System.Windows.Forms
Imports CapEdit.Helpers.Pictures
Imports CapEdit.Settings

Public Class UcList

#Region " Fields "
    Public ScreenBitmapFile As String = ""

    Private m_AnimInProgress As Boolean
    Private m_ReadyToDelete As Boolean
    Private m_ScreenBitmap As Bitmap
    Private m_OneSelectedElement As Boolean
    Private ReadOnly m_Parameters As Parameters
#End Region

#Region " Properties "
    Public Property ScreenBitmap() As Bitmap
        Get
            Return m_ScreenBitmap
        End Get
        Set(ByVal value As Bitmap)
            m_ScreenBitmap = value
            CopyImageToClipboard(m_ScreenBitmap)
        End Set
    End Property

    Public Property ListviewList() As ListviewEx
        Get
            Return LvList
        End Get
        Set(ByVal value As ListviewEx)
            LvList = value
        End Set
    End Property

    Public Property PcbPictureList() As PictureBox
        Get
            Return PcbListPicture
        End Get
        Set(ByVal value As PictureBox)
            PcbListPicture = value
        End Set
    End Property

    Public Property TsBtnEditList() As ToolStripButton
        Get
            Return TsBtnListEdit
        End Get
        Set(ByVal value As ToolStripButton)
            TsBtnListEdit = value
        End Set
    End Property

    Public Property TsBtnRemoveFileList() As ToolStripButton
        Get
            Return TsBtnListRemoveFile
        End Get
        Set(ByVal value As ToolStripButton)
            TsBtnListRemoveFile = value
        End Set
    End Property

    Public Property TsBtnSaveAsList() As ToolStripButton
        Get
            Return TsBtnListSaveAs
        End Get
        Set(ByVal value As ToolStripButton)
            TsBtnListSaveAs = value
        End Set
    End Property

    Public Property TsBtnAddFileList() As ToolStripButton
        Get
            Return TsBtnListAddFile
        End Get
        Set(ByVal value As ToolStripButton)
            TsBtnListAddFile = value
        End Set
    End Property

    Public Property BgwFilesAdd() As BackgroundWorker
        Get
            Return BgwAddFiles
        End Get
        Set(ByVal value As BackgroundWorker)
            BgwAddFiles = value
        End Set
    End Property
#End Region

#Region " Events "
    Public Event OnCaptureNowButtonClick(sender As Object, e As EventArgs)
    Public Event OnEditButtonClick(sender As Object, e As EventArgs)
    Public Event OnGifAnimationTextChanged(sender As Object, text As String)
#End Region

#Region " Constructor "
    Public Sub New(Parameters As Parameters)
        m_Parameters = Parameters
        InitializeComponent()
        Dock = DockStyle.Fill
        If File.Exists(Parameters.Options.EditApp) Then
            TsBtnListEdit.Image = Icon.ExtractAssociatedIcon(Parameters.Options.EditApp).ToBitmap()
        End If
    End Sub
#End Region


#Region "LIST ADDFILE"

    Private Sub LvList_DragOver(sender As Object, e As DragEventArgs) Handles LvList.DragOver
        Dim validData As Boolean
        Dim filename = String.Empty
        validData = GetFilename(filename, e)
        If validData Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Function GetFilename(ByRef filename As String, e As DragEventArgs) As Boolean
        Dim ret As Boolean = False
        filename = String.Empty
        If (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy Then
            Dim data As Array = TryCast(e.Data.GetData("FileName"), Array)
            If data IsNot Nothing Then
                If (data.Length = 1) AndAlso (TypeOf data.GetValue(0) Is String) Then
                    filename = DirectCast(data, String())(0)
                    Dim ext = Path.GetExtension(filename).ToLower()
                    If (ext = Extension.bmpExt) OrElse (ext = Extension.gifExt) OrElse (ext = Extension.jpgExt) OrElse (ext = Extension.jpegExt) OrElse (ext = Extension.pngExt) Then
                        If Utils.CorrectMIMEType(filename) Then
                            ret = True
                        End If
                    End If
                End If
            End If
        End If
        Return ret
    End Function

    Private Sub LvList_DragDrop(sender As Object, e As DragEventArgs) Handles LvList.DragDrop
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            If Not BgwAddFiles.IsBusy Then
                BgwAddFiles.RunWorkerAsync(e.Data.GetData(DataFormats.FileDrop))
            End If
        End If
    End Sub

    'Private Sub LvList_ElevatedDragDrop(sender As Object, e As ElevatedDragDropArgs)
    '    If e.HWnd = LvList.Handle Then
    '        Dim files As New List(Of String)
    '        For Each file As String In e.Files
    '            If GetFilename(file) Then
    '                files.Add(file)
    '            End If
    '        Next
    '        If Not BgwAddFiles.IsBusy Then
    '            BgwAddFiles.RunWorkerAsync(files.ToArray)
    '        End If
    '    End If
    'End Sub

    'Private Function GetFilename(filename As String) As Boolean
    '    If Not ElevatedUtils.isDirectory(filename) Then
    '        Dim fi As New FileInfo(filename)
    '        If fi.Length <> 0 Then
    '            Dim ext = fi.Extension.ToLower
    '            If (ext = Extension.bmpExt) OrElse (ext = Extension.gifExt) OrElse (ext = Extension.jpgExt) OrElse (ext = Extension.jpegExt) OrElse (ext = Extension.pngExt) Then
    '                If Utils.CorrectMIMEType(filename) Then
    '                    Return True
    '                End If
    '            End If
    '        End If
    '    End If
    '    Return False
    'End Function

    Private Sub TsBtnListAddFile_Click(sender As Object, e As EventArgs) Handles TsBtnListAddFile.Click
        Using ofd As New OpenFileDialog
            With ofd
                .Filter = "Images|*.bmp;*.gif;*.jpg;*.jpeg;*.png"
                .Title = "Sélectionnez un ou plusieurs fichiers"
                .CheckFileExists = True
                .Multiselect = True
                If .ShowDialog() = DialogResult.OK Then
                    BgwAddFiles.RunWorkerAsync(.FileNames)
                End If
            End With
        End Using
    End Sub

    Private Sub BgwAddFiles_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwAddFiles.DoWork
        If TypeOf e.Argument Is String() Then
            For Each f In CType(e.Argument, String())
                PopulateList(f)
            Next
        Else
            PopulateList(e.Argument)
        End If
    End Sub

    Private Sub PopulateList(FilePath$)
        If File.Exists(FilePath) Then
            Dim fi As New FileInfo(FilePath)
            If fi.Length <> 0 Then
                If Utils.CorrectMIMEType(fi.FullName) Then
                    Dim items As String() = New String(3) {}
                    items(0) = fi.Name
                    items(1) = fi.FullName
                    items(2) = Utils.BytestoString(fi.Length)
                    Dim item As New ListViewItem(items) With {
                        .ToolTipText = items(1)
                    }
                    Dim ic As Icon = Icons.IconFromExtensionUsingRegistry(fi.Extension, Icons.SystemIconSize.Large)
                    BgwAddFiles.ReportProgress(Nothing, Tuple.Create(item, ic))
                End If
            End If
        End If
    End Sub

    Private Sub BgwAddFiles_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwAddFiles.ProgressChanged
        Dim tups = TryCast(e.UserState, Tuple(Of ListViewItem, Icon))
        Dim lvi As ListViewItem = tups.Item1
        Dim Ico As Icon = tups.Item2
        If Not IsInCollection(lvi) Then
            If Not Ico Is Nothing Then
                ImageListList.Images.Add(Ico)
                lvi.ImageIndex = ImageListList.Images.Count - 1
            End If
            LvList.ItemsAddRange(New ListViewItem() {lvi})
        End If
    End Sub

    Private Function IsInCollection(lvi As ListViewItem) As Boolean
        Return LvList.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(1).Text.ToLower = lvi.SubItems(1).Text.ToLower)
    End Function

    Private Sub BgwAddFiles_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwAddFiles.RunWorkerCompleted
        SelectedLastAdded(LvList)
    End Sub

    Private Sub SelectedLastAdded(lv As ListView)
        Dim iCount% = lv.Items.Count
        For Each it As ListViewItem In lv.Items
            If Not it.Index = iCount - 1 Then
                it.Selected = False
                ScreenBitmap = Nothing
            Else
                it.Selected = True
                ScreenBitmap = Utils.ImageFromFile(it.SubItems(1).Text)
                Exit For
            End If
        Next
    End Sub

    Private Sub Pcblistpicture_SizeChanged(sender As Object, e As EventArgs) Handles PcbListPicture.SizeChanged
        Utils.AutosizeImg(ScreenBitmapFile, PcbListPicture, PictureBoxSizeMode.CenterImage)
    End Sub

#End Region

#Region "LIST REMOVEFILE"

    Private Sub LvList_KeyDown(sender As Object, e As KeyEventArgs) Handles LvList.KeyDown
        If Not LvList.SelectedItems.Count = 0 Then
            If e.KeyCode = Keys.Delete Then DelSelectedLv()
        End If
    End Sub

    Private Sub TsBtnListRemoveFile_Click(sender As Object, e As EventArgs) Handles TsBtnListRemoveFile.Click
        DelSelectedLv()
    End Sub

    Private Sub DelSelectedLv()
        If MessageBox.Show("Etes-vous certains de vouloir supprimer " & If(LvList.SelectedItems.Count > 1, "ces éléments", "cet élément"),
                        "Suppression de la liste", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Try
                Dim MostVal = LvList.SelectedIndices.Cast(Of Integer).Max

                Dim LviPath As String = ""
                If Not LvList.SelectedItems.Count = LvList.Items.Count Then
                    If (MostVal + 1) > LvList.Items.Count - 1 Then
                        For i As Integer = MostVal To 0 Step -1
                            If Not LvList.SelectedIndices.Contains(LvList.Items(i).Index) Then
                                LviPath = LvList.Items(i).SubItems(1).Text
                                Exit For
                            End If
                        Next
                    Else
                        LviPath = LvList.Items(MostVal + 1).SubItems(1).Text
                    End If
                End If

                For Each lvi As ListViewItem In LvList.SelectedItems
                    LvList.RemoveLvi(lvi)
                Next
                If LvList.Items.Count < 1 Then
                    GbxPreview.Enabled = False
                Else
                    If Not LviPath = "" Then
                        LvList.Items.Cast(Of ListViewItem).Where(Function(f) f.SubItems(1).Text = LviPath).FirstOrDefault.Selected = True
                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub

#End Region

#Region "LIST EDITER"

    Private Sub TsBtnListEdit_Click(sender As Object, e As EventArgs) Handles TsBtnListEdit.Click
        RaiseEvent OnEditButtonClick(sender, e)
    End Sub

    Private Sub TsBtnListCapture_Click(sender As Object, e As EventArgs) Handles TsBtnListCapture.Click
        RaiseEvent OnCaptureNowButtonClick(sender, e)
    End Sub

    Private Sub LvList_DoubleClick(sender As Object, e As EventArgs) Handles LvList.DoubleClick
        If m_OneSelectedElement Then
            RaiseEvent OnEditButtonClick(sender, e)
        End If
    End Sub

#End Region

#Region "LIST SAVEAS"

    Private Sub TsBtnListSaveAs_Click(sender As Object, e As EventArgs) Handles TsBtnListSaveAs.Click
        SavePicture()
    End Sub

    Private Sub SavePicture()
        If m_ReadyToDelete Then
            Using fbd As New FolderBrowserDialog
                With fbd
                    .Description = "Sélectionnez un répertoire de sauvegarde"
                    If .ShowDialog = DialogResult.OK Then
                        Dim SelectedPath = .SelectedPath
                        Dim Items As New List(Of ListViewItem)
                        For Each it As ListViewItem In LvList.SelectedItems
                            Items.Add(it)
                        Next
                        Dim wait As New Thread(New ThreadStart(Sub()
                                                                   For Each it In Items
                                                                       Try
                                                                           'Dim fName = Date.Now.ToString("yyyyMMddHHmmssffffff") & "." & it.Text.Split(".")(1)
                                                                           Dim fi As New FileInfo(it.SubItems(1).Text)
                                                                           File.Copy(it.SubItems(1).Text, Path.Combine(SelectedPath, fi.Name))
                                                                       Catch ex As Exception
                                                                           'MsgBox(ex.ToString, MsgBoxStyle.Exclamation)
                                                                       End Try
                                                                   Next
                                                                   Items.Clear()
                                                               End Sub))
                        wait.Start()
                    End If
                End With
            End Using
        Else
            Using sfd As New SaveFileDialog
                With sfd
                    .Filter = "PNG (*.png)|*.png|BMP (*.bmp)|*.bmp|JPG (*.jpg)|*.jpg|JPEG (*.jpeg)|*.jpeg|GIF (*.gif)|*.gif|TIF (*.tif)|*.tif|TIFF (*.tiff)|*.tiff"
                    .Title = "Enregistrer votre fichier sous ..."
                    .OverwritePrompt = True
                    Dim fi As New FileInfo(ScreenBitmapFile)
                    .FileName = fi.Name.Replace(fi.Extension, "")
                    If .ShowDialog() = DialogResult.OK Then
                        Dim f As New FileInfo(.FileName)
                        Try
                            Dim imag = Utils.ImageFromFile(ScreenBitmapFile)
                            Utils.SavePicture(imag, f.FullName, f.Extension.ToLower, m_Parameters.Options.JpegCompression)
                            imag.Dispose()
                        Catch ex As Exception
                            'MsgBox(ex.Message, MsgBoxStyle.Information)
                        End Try
                    End If
                End With
            End Using
        End If

    End Sub

#End Region

#Region "LIST METHODS"

    Private Sub LvList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LvList.SelectedIndexChanged
        If LvList.SelectedItems.Count > 0 Then

            Dim img = CopyImageFromFileToClipboard(LvList.SelectedItems(0).SubItems(1).Text)
            Utils.AutosizeImg(img, PcbListPicture)

            If LvList.SelectedItems.Count = 1 Then
                m_OneSelectedElement = True
                If m_AnimInProgress = False Then
                    EnableListMenu(True)

                    If LvList.SelectedItems(0).Index = 0 Then
                        TsBtnListUp.Enabled = False
                    Else
                        TsBtnListUp.Enabled = True
                    End If
                    If LvList.SelectedItems(0).Index = LvList.Items.Count - 1 Then
                        TsBtnListDown.Enabled = False
                    Else
                        TsBtnListDown.Enabled = True
                    End If
                Else
                    TsBtnListSaveAnimation.Enabled = True
                End If
                m_ReadyToDelete = False
            Else
                m_OneSelectedElement = False
                TsBtnListDown.Enabled = False
                TsBtnListUp.Enabled = False
                TsBtnListEdit.Enabled = False
                TsBtnListRemoveFile.Enabled = True
                'PcbListPicture.Image = Nothing
                GbxPreview.Enabled = False
                'ScreenBitmapFile = String.Empty
                'ScreenBitmap = Nothing
                m_ReadyToDelete = True
                TsBtnListSaveAnimation.Enabled = True
            End If
        Else
            m_ReadyToDelete = False
            m_OneSelectedElement = False
            TsBtnListDown.Enabled = False
            TsBtnListUp.Enabled = False
            EnableListMenu(False)
        End If
    End Sub

    Private Sub EnableListMenu(val As Boolean)
        If val Then
            TsBtnListEdit.Enabled = True
            TsBtnListRemoveFile.Enabled = val
            TsBtnListSaveAs.Enabled = val
            GbxPreview.Enabled = val
            TsBtnListDown.Enabled = val
            TsBtnListUp.Enabled = val
            TsBtnListSaveAnimation.Enabled = False
        Else
            TsBtnListEdit.Enabled = val
            TsBtnListRemoveFile.Enabled = val
            TsBtnListSaveAs.Enabled = val
            PcbListPicture.Image = Nothing
            GbxPreview.Enabled = val
            ScreenBitmapFile = String.Empty
            ScreenBitmap = Nothing
            TsBtnListDown.Enabled = val
            TsBtnListUp.Enabled = val
            If m_AnimInProgress Then
                TsBtnListSaveAnimation.Enabled = True
            Else
                TsBtnListSaveAnimation.Enabled = val
            End If
        End If
    End Sub

    Public Function CopyImageFromFileToClipboard(filePathFromLvi As String) As Bitmap
        ScreenBitmapFile = filePathFromLvi
        If File.Exists(ScreenBitmapFile) Then
            If Utils.GetMIMEType(ScreenBitmapFile) = "image/gif" Then
                ScreenBitmap = Utils.GetGifFirstFrame(ScreenBitmapFile)
            Else
                ScreenBitmap = Utils.ImageFromFile(ScreenBitmapFile)
            End If
        End If
        PcbListPicture.Tag = ScreenBitmap
        My.Computer.Clipboard.SetImage(ScreenBitmap)
        Return ScreenBitmap
    End Function

    Public Sub CopyImageToClipboard(img As Bitmap)
        PcbListPicture.Tag = img
        If Not img Is Nothing Then
            My.Computer.Clipboard.SetImage(img)
        End If
    End Sub


    Private Sub MoveListViewItem(lv As ListviewEx, moveUp As Boolean)
        Dim i%
        Dim cache$
        Dim selIdx%
        Dim checked As Boolean
        Dim lastDatas As Object = Nothing
        Dim toolTipTextData As String = String.Empty
        Dim imgKey As Integer

        With lv
            selIdx = .SelectedItems.Item(0).Index
            checked = .SelectedItems.Item(0).Checked
            lastDatas = .SelectedItems.Item(0).Tag
            toolTipTextData = .SelectedItems.Item(0).ToolTipText
            imgKey = .SelectedItems.Item(0).ImageIndex

            If moveUp Then
                ' On ignore le mouvement vers le haut car l'Index est égal à 0
                If selIdx = 0 Then Exit Sub
                ' on déplace vers le haut le subitems de la ligne précédente
                For i = 0 To .Items(selIdx).SubItems.Count - 1
                    cache = .Items(selIdx - 1).SubItems(i).Text
                    .Items(selIdx - 1).SubItems(i).Text = .Items(selIdx).SubItems(i).Text
                    .Items(selIdx).SubItems(i).Text = cache
                Next

                Dim cacheImgKey = .Items(selIdx - 1).ImageIndex
                .Items(selIdx - 1).ImageIndex = imgKey
                .Items(selIdx).ImageIndex = cacheImgKey

                Dim cacheToolTipTextData = .Items(selIdx - 1).ToolTipText
                .Items(selIdx - 1).ToolTipText = toolTipTextData
                .Items(selIdx).ToolTipText = cacheToolTipTextData

                Dim cacheLastDatas = .Items(selIdx - 1).Tag
                .Items(selIdx - 1).Tag = lastDatas
                .Items(selIdx).Tag = cacheLastDatas

                Dim CacheChecked = .Items(selIdx - 1).Checked
                .Items(selIdx - 1).Checked = checked
                .Items(selIdx).Checked = CacheChecked

                For Each it As ListViewItem In LvList.Items
                    it.Selected = False
                Next

                .Items(selIdx - 1).Selected = True

                .Focus()
                If selIdx = 1 Then
                    ' On passe le bouton permettant de naviguer vers le haut à l'état Disabled
                    TsBtnListUp.Enabled = False
                End If
            Else
                ' On ignore le mouvement vers le bas car l'index est égal au nombre d'éléments présents dans le listview
                If selIdx = .Items.Count - 1 Then Exit Sub
                For i = 0 To .Items(selIdx).SubItems.Count - 1
                    cache = .Items(selIdx + 1).SubItems(i).Text
                    .Items(selIdx + 1).SubItems(i).Text = .Items(selIdx).SubItems(i).Text
                    .Items(selIdx).SubItems(i).Text = cache
                Next

                Dim cacheImgKey = .Items(selIdx + 1).ImageIndex
                .Items(selIdx + 1).ImageIndex = imgKey
                .Items(selIdx).ImageIndex = cacheImgKey

                Dim cacheToolTipTextData = .Items(selIdx + 1).ToolTipText
                .Items(selIdx + 1).ToolTipText = toolTipTextData
                .Items(selIdx).ToolTipText = cacheToolTipTextData

                Dim cacheLastDatas = .Items(selIdx + 1).Tag
                .Items(selIdx + 1).Tag = lastDatas
                .Items(selIdx).Tag = cacheLastDatas

                Dim CacheChecked = .Items(selIdx + 1).Checked
                .Items(selIdx + 1).Checked = checked
                .Items(selIdx).Checked = CacheChecked

                For Each it As ListViewItem In LvList.Items
                    it.Selected = False
                Next

                .Items(selIdx + 1).Selected = True

                .Focus()
                If selIdx = .Items.Count - 2 Then
                    ' On passe le bouton permettant de naviguer vers le bas à l'état Disabled
                    TsBtnListDown.Enabled = False
                End If
            End If
        End With
    End Sub

    Private Sub TsBtnListUp_Click(sender As Object, e As EventArgs) Handles TsBtnListUp.Click
        MoveListViewItem(LvList, True)
    End Sub

    Private Sub TsBtnListDown_Click(sender As Object, e As EventArgs) Handles TsBtnListDown.Click
        MoveListViewItem(LvList, False)
    End Sub

    Private Sub LvList_ItemsCountChanged() Handles LvList.ItemChanged
        If LvList.Items.Count > 0 Then
        Else
            TsBtnListUp.Enabled = False
            TsBtnListDown.Enabled = False
        End If
    End Sub

    Private Sub TsBtnListSaveAnimation_Click(sender As Object, e As EventArgs) Handles TsBtnListSaveAnimation.Click
        If Not BgwGifAnimationStart.IsBusy Then

            TryCast(TryCast(TsBtnListSaveAnimation.Owner.Parent.Parent.Parent.Parent.Parent, TabControl).Parent.Parent, SplitContainer).Panel1.Controls("UcMainMenu1").Enabled = False

            LvList.Enabled = False
            'UcMainMenu1.MenuEnabled = False
            TsBtnListAddFile.Enabled = False
            TsBtnListRemoveFile.Enabled = False
            TsBtnListCapture.Enabled = False
            TsBtnListDown.Enabled = False
            TsBtnListUp.Enabled = False
            TsBtnListSaveAs.Enabled = False
            TsBtnListSaveAnimation.Enabled = True

            TsBtnListSaveAnimation.Text = "Annuler Gif animé !"

            m_AnimInProgress = True

            Dim Widths As New List(Of Integer)
            Dim Heights As New List(Of Integer)

            Dim str As New List(Of String)
            For Each lvi As ListViewItem In LvList.SelectedItems
                str.Add(lvi.SubItems(1).Text)
                Dim img = Utils.ImageFromFile(lvi.SubItems(1).Text)
                Widths.Add(img.Width)
                Heights.Add(img.Height)
                img.Dispose()
                lvi.Selected = False
            Next

            Dim sizes = New Size(Widths.Max(), Heights.Max())
            Dim obj As Object() = New Object() {str, m_Parameters.Options.GifDelay, sizes, m_Parameters.Options.GifBorder}
            BgwGifAnimationStart.RunWorkerAsync(obj)
        Else
            BgwGifAnimationStart.CancelAsync()
        End If
    End Sub

    Private Sub BgwGifAnimationStart_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwGifAnimationStart.DoWork
        Try
            Dim borderSize = 0
            Dim lvis = TryCast(e.Argument(0), List(Of String))
            Dim Delay = CInt(e.Argument(1))
            Dim sizes As Size = e.Argument(2)
            Dim border = CBool(e.Argument(3))
            Dim lviCount As Integer = lvis.Count
            Dim i As Integer = 0
            If border Then borderSize = 1

            BgwGifAnimationStart.ReportProgress(200, "CapEdit - Création de l'animation - 0%")

            Dim m_GifoutputPath = Path.Combine(Path.GetTempPath, Date.Now.ToString("yyyyMMddHHmmssffffff") & ".gif")
            Using gif = AnimatedGif.AnimatedGif.Create(m_GifoutputPath, Delay)
                For Each lvi In lvis
                    If BgwGifAnimationStart.CancellationPending Then
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim fi As New FileInfo(lvi)
                    Dim img = Utils.ImageFromFile(fi.FullName)
                    If border Then
                        img = Utils.DrawBitmapWithBorder(img, borderSize)
                    End If
                    Dim scale = Utils.ScaleImage(img, sizes.Width + (borderSize * 2), sizes.Height + (borderSize * 2))
                    gif.AddFrame(scale)
                    img.Dispose()
                    scale.Dispose()
                    i += 1
                    BgwGifAnimationStart.ReportProgress((i * 100) / lviCount, fi.FullName)
                Next
            End Using
            e.Result = New String() {"True", m_GifoutputPath}
        Catch ex As Exception
            e.Result = New String() {"False", ex.Message}
        End Try
    End Sub

    Private Sub BgwGifAnimationStart_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwGifAnimationStart.ProgressChanged
        If e.ProgressPercentage = 200 Then
            RaiseEvent OnGifAnimationTextChanged(Me, e.UserState.ToString)
        Else
            If Not e.ProgressPercentage > 100 Then
                RaiseEvent OnGifAnimationTextChanged(Me, "CapEdit - Création de l'animation - " & e.ProgressPercentage.ToString & "%")
                Dim currentLvi = LvList.Items.Cast(Of ListViewItem).Where(Function(f) f.SubItems(1).Text = e.UserState.ToString).First
                Dim currentLviIndex = currentLvi.Index
                If currentLviIndex >= 1 Then
                    LvList.Items(currentLviIndex - 1).Selected = False
                End If
                currentLvi.Selected = True
            End If
        End If
    End Sub

    Private Sub BgwGifAnimationStart_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwGifAnimationStart.RunWorkerCompleted
        If e.Cancelled Then
            RaiseEvent OnGifAnimationTextChanged(Me, "CapEdit - Création de l'animation annulée !")
            MessageBox.Show("La création de l'animation a été annulée !", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If e.Result(0).ToString = True Then
                RaiseEvent OnGifAnimationTextChanged(Me, "CapEdit - Création de l'animation - 100%")
                Using sfd As New SaveFileDialog
                    With sfd
                        .Filter = "GIF (*.gif)|*.gif"
                        .Title = "Enregistrer votre fichier sous ..."
                        .OverwritePrompt = True
                        Dim fi As New FileInfo(e.Result(1).ToString)
                        .FileName = fi.Name.Replace(fi.Extension, "")
                        If .ShowDialog() = DialogResult.OK Then
                            File.Copy(e.Result(1).ToString, .FileName, True)
                            MessageBox.Show("L'animation Gif a été enregistré avec succès !", "Opération réussie", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End With
                End Using

            Else
                RaiseEvent OnGifAnimationTextChanged(Me, "CapEdit - Erreur lors de la réation de l'animation !")
                MessageBox.Show("Une erreur est survenue lors de la création de l'animation !", "Echec de l'opération", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If

        RaiseEvent OnGifAnimationTextChanged(Me, "CapEdit")
        TsBtnListSaveAnimation.Text = "Créer Gif animé ..."

        m_AnimInProgress = False
        TryCast(TryCast(TsBtnListSaveAnimation.Owner.Parent.Parent.Parent.Parent.Parent, TabControl).Parent.Parent, SplitContainer).Panel1.Controls("UcMainMenu1").Enabled = True
        'UcMainMenu1.MenuEnabled = True
        LvList.Enabled = True
        TsBtnListAddFile.Enabled = True
        TsBtnListRemoveFile.Enabled = True
        TsBtnListCapture.Enabled = True
        TsBtnListEdit.Enabled = True
        TsBtnListDown.Enabled = False
        TsBtnListUp.Enabled = True
        TsBtnListSaveAs.Enabled = True
        TsBtnListSaveAnimation.Enabled = False
    End Sub

    Private Sub LvList_ItemChanged(sender As Object, e As EventArgs) Handles LvList.ItemChanged

    End Sub

    Private Sub LvList_ItemsCountChanged(sender As Object, e As EventArgs) Handles LvList.ItemChanged

    End Sub



#End Region

End Class
