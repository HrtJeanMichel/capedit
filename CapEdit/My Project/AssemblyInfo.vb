﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("CapEdit")>
<Assembly: AssemblyDescription("CapEdit")>
<Assembly: AssemblyCompany("Hrtjm")>
<Assembly: AssemblyProduct("CapEdit")>
<Assembly: AssemblyCopyright("Copyright © Hrtjm 2008-2020")>
<Assembly: AssemblyTrademark("Hrtjm")>

<Assembly: ComVisible(False)>

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("6015fb8a-d55a-4eb0-9fec-1bce9abdcf85")>

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.3.0.0")>
<Assembly: AssemblyFileVersion("3.3.0.0")>
