﻿Imports System.Threading
Imports CapEdit.Settings

Friend Class Program

#Region " Fields "
    Private Shared m_settings As Parameters
#End Region

#Region " Constructor "
    Shared Sub New()
        m_settings = New Parameters
    End Sub
#End Region

#Region " Methods "
    <STAThread()>
    Friend Shared Sub Main(Args$())
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)

        Dim instanceCountOne As Boolean = False
        Using mtex As Mutex = New Mutex(True, Application.ProductName, instanceCountOne)
            If instanceCountOne Then
                Parameters.Initialize(Application.StartupPath, m_settings)
                Application.Run(New FrmMain(m_settings))
                mtex.ReleaseMutex()
            End If
        End Using
    End Sub
#End Region

End Class
