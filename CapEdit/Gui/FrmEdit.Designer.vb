﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmEdit
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SpcEditMain = New System.Windows.Forms.SplitContainer()
        Me.UcColor1 = New CapEdit.Controls.UcColor()
        Me.TsEdit = New System.Windows.Forms.ToolStrip()
        Me.TsBtnReload = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnUndo = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnRectangle = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsBtnRectangleCrop = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsCbxPenSize = New System.Windows.Forms.ToolStripComboBox()
        Me.PcbEdit = New System.Windows.Forms.PictureBox()
        CType(Me.SpcEditMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcEditMain.Panel1.SuspendLayout()
        Me.SpcEditMain.Panel2.SuspendLayout()
        Me.SpcEditMain.SuspendLayout()
        Me.TsEdit.SuspendLayout()
        CType(Me.PcbEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SpcEditMain
        '
        Me.SpcEditMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcEditMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcEditMain.IsSplitterFixed = True
        Me.SpcEditMain.Location = New System.Drawing.Point(0, 0)
        Me.SpcEditMain.Name = "SpcEditMain"
        Me.SpcEditMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcEditMain.Panel1
        '
        Me.SpcEditMain.Panel1.Controls.Add(Me.UcColor1)
        Me.SpcEditMain.Panel1.Controls.Add(Me.TsEdit)
        '
        'SpcEditMain.Panel2
        '
        Me.SpcEditMain.Panel2.AutoScroll = True
        Me.SpcEditMain.Panel2.Controls.Add(Me.PcbEdit)
        Me.SpcEditMain.Size = New System.Drawing.Size(793, 577)
        Me.SpcEditMain.SplitterDistance = 28
        Me.SpcEditMain.TabIndex = 0
        '
        'UcColor1
        '
        Me.UcColor1.Location = New System.Drawing.Point(222, 0)
        Me.UcColor1.Name = "UcColor1"
        Me.UcColor1.SelectedColor = System.Drawing.Color.Red
        Me.UcColor1.Size = New System.Drawing.Size(169, 25)
        Me.UcColor1.TabIndex = 1
        '
        'TsEdit
        '
        Me.TsEdit.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsEdit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsBtnReload, Me.ToolStripSeparator4, Me.TsBtnUndo, Me.ToolStripSeparator1, Me.TsBtnRectangle, Me.ToolStripSeparator2, Me.TsBtnRectangleCrop, Me.ToolStripSeparator3, Me.TsCbxPenSize})
        Me.TsEdit.Location = New System.Drawing.Point(0, 0)
        Me.TsEdit.Name = "TsEdit"
        Me.TsEdit.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.TsEdit.ShowItemToolTips = False
        Me.TsEdit.Size = New System.Drawing.Size(793, 25)
        Me.TsEdit.TabIndex = 0
        '
        'TsBtnReload
        '
        Me.TsBtnReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnReload.Image = Global.CapEdit.My.Resources.Resources.Reload
        Me.TsBtnReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnReload.Name = "TsBtnReload"
        Me.TsBtnReload.Size = New System.Drawing.Size(23, 22)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnUndo
        '
        Me.TsBtnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnUndo.Enabled = False
        Me.TsBtnUndo.Image = Global.CapEdit.My.Resources.Resources.undo
        Me.TsBtnUndo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnUndo.Name = "TsBtnUndo"
        Me.TsBtnUndo.Size = New System.Drawing.Size(23, 22)
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnRectangle
        '
        Me.TsBtnRectangle.Checked = True
        Me.TsBtnRectangle.CheckOnClick = True
        Me.TsBtnRectangle.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TsBtnRectangle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnRectangle.Image = Global.CapEdit.My.Resources.Resources.rectangle
        Me.TsBtnRectangle.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRectangle.Name = "TsBtnRectangle"
        Me.TsBtnRectangle.Size = New System.Drawing.Size(23, 22)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'TsBtnRectangleCrop
        '
        Me.TsBtnRectangleCrop.CheckOnClick = True
        Me.TsBtnRectangleCrop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnRectangleCrop.Image = Global.CapEdit.My.Resources.Resources.Crop
        Me.TsBtnRectangleCrop.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnRectangleCrop.Name = "TsBtnRectangleCrop"
        Me.TsBtnRectangleCrop.Size = New System.Drawing.Size(23, 22)
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'TsCbxPenSize
        '
        Me.TsCbxPenSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TsCbxPenSize.Items.AddRange(New Object() {"Trait discret", "Trait fin", "Trait moyen", "Trait épais", "Trait énorme"})
        Me.TsCbxPenSize.Name = "TsCbxPenSize"
        Me.TsCbxPenSize.Size = New System.Drawing.Size(100, 25)
        '
        'PcbEdit
        '
        Me.PcbEdit.Location = New System.Drawing.Point(0, 0)
        Me.PcbEdit.Name = "PcbEdit"
        Me.PcbEdit.Size = New System.Drawing.Size(793, 538)
        Me.PcbEdit.TabIndex = 0
        Me.PcbEdit.TabStop = False
        '
        'FrmEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(793, 577)
        Me.Controls.Add(Me.SpcEditMain)
        Me.MinimizeBox = False
        Me.Name = "FrmEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Editeur simplifié"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SpcEditMain.Panel1.ResumeLayout(False)
        Me.SpcEditMain.Panel1.PerformLayout()
        Me.SpcEditMain.Panel2.ResumeLayout(False)
        CType(Me.SpcEditMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcEditMain.ResumeLayout(False)
        Me.TsEdit.ResumeLayout(False)
        Me.TsEdit.PerformLayout()
        CType(Me.PcbEdit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SpcEditMain As SplitContainer
    Friend WithEvents PcbEdit As PictureBox
    Friend WithEvents TsEdit As ToolStrip
    Friend WithEvents TsBtnUndo As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents TsBtnRectangle As ToolStripButton
    Friend WithEvents TsCbxPenSize As ToolStripComboBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents TsBtnRectangleCrop As ToolStripButton
    Friend WithEvents UcColor1 As Controls.UcColor
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents TsBtnReload As ToolStripButton
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
End Class
