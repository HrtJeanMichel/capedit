﻿Imports System.IO
Imports System.Threading
Imports CapEdit.Settings
Imports CapEdit.Core.Screenshot
Imports CapEdit.Core.Editor
Imports CapEdit.Helpers.Processus
Imports CapEdit.Helpers.Pictures
Imports CapEdit.Controls

Friend Class FrmMain

#Region "FRM_MAIN VARIABLES"
    Private WithEvents CapScreen As CaptureScreen

    Private m_parameters As Parameters
    Private ReadOnly m_ucOptions As UcOptions
    Private ReadOnly m_ucList As UcList
    Private ReadOnly m_AnimInProgress As Boolean

    Private m_edit As Watcher
    Private Delegate Sub ShowingMainForm()
    Private Delegate Sub HiddingMainForm()

    Private m_editFile As Boolean
    Private m_clipboardImg As Boolean
    Private ReadOnly m_enableClientSizeChanged As Boolean
#End Region

#Region "FRM_MAIN METHODS"

    Friend Sub New(Parameters As Parameters)
        m_parameters = Parameters
        InitializeComponent()

        HideMainMenuTabs()
        PopulateMonitor()

        TpMainAbout.Controls.Add(New UcAbout)

        m_ucOptions = New UcOptions(Handle, Parameters)
        TpMainOptions.Controls.Add(m_ucOptions)

        m_ucList = New UcList(Parameters)
        AddHandler m_ucList.OnCaptureNowButtonClick, AddressOf OnCaptureNowButton_Click
        AddHandler m_ucList.OnEditButtonClick, AddressOf OnEditButton_Click
        AddHandler m_ucList.OnGifAnimationTextChanged, AddressOf OnGifAnimationText_Changed
        TpMainList.Controls.Add(m_ucList)

        m_enableClientSizeChanged = True

    End Sub

    Private Sub HideMainMenuTabs()
        With TbcMain
            .SuspendLayout()
            .SizeMode = TabSizeMode.Fixed
            .ItemSize = New Size(0, 1)
            .Appearance = TabAppearance.Buttons
            .ResumeLayout()
        End With
    End Sub

    Private Sub PopulateMonitor()
        Dim l As New List(Of Screen)
        Dim iScreen As Integer = 0
        For Each scr In Screen.AllScreens
            l.Add(scr)
            iScreen += 1
            If iScreen = 2 Then
                With TsmCaptureMonitor1
                    .Visible = True
                    .Tag = l(0)
                End With
                With TsmCaptureMonitor2
                    .Visible = True
                    .Tag = l(1)
                End With
            ElseIf iScreen = 3 Then
                With TsmCaptureMonitor1
                    .Visible = True
                    .Tag = l(0)
                End With
                With TsmCaptureMonitor2
                    .Visible = True
                    .Tag = l(1)
                End With
                With TsmCaptureMonitor3
                    .Visible = True
                    .Tag = l(2)
                End With
            End If
        Next
    End Sub

    Private Sub Frm_Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dim wait As New Thread(New ThreadStart(Sub()
                                                   Try
                                                       For Each fi As String In Directory.GetFiles(My.Computer.FileSystem.SpecialDirectories.Temp)
                                                           Dim f As New FileInfo(fi)
                                                           If (f.Extension.ToLower = Extension.bmpExt) OrElse (f.Extension.ToLower = Extension.gifExt) OrElse (f.Extension.ToLower = Extension.jpgExt) OrElse (f.Extension.ToLower = Extension.jpegExt) OrElse (f.Extension.ToLower = Extension.pngExt) Then
                                                               File.Delete(fi)
                                                           End If
                                                       Next
                                                   Catch ex As Exception
                                                   End Try
                                               End Sub))
        wait.Start()
    End Sub

    Private Sub Frm_Main_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If WindowState = FormWindowState.Minimized Then
            If m_editFile Then
                'NotifyNow("L'image est prête à être modifée depuis votre logiciel d'édition !")
            Else
                If m_clipboardImg Then
                    'NotifyNow("L'image a été copiée dans le presse-papier !")
                    m_clipboardImg = False
                Else
                    NtiSystray.Visible = True
                End If
            End If
        ElseIf WindowState = FormWindowState.Normal Then
            If m_editFile Then
                WindowState = FormWindowState.Minimized
            Else
                NtiSystray.Visible = False
                'TopMost = True : TopMost = False
            End If
        End If
    End Sub

    Private Sub FrmMain_ClientSizeChanged(sender As Object, e As EventArgs) Handles MyBase.ClientSizeChanged
        If m_enableClientSizeChanged Then
            Dim oldTotalWidth = 0
            For Each column As ColumnHeader In m_ucList.ListviewList.Columns
                oldTotalWidth += column.Width
            Next
            Dim newTotalWidth = m_ucList.ListviewList.ClientSize.Width
            Dim growthFactor As Double = newTotalWidth / oldTotalWidth
            For Each column As ColumnHeader In m_ucList.ListviewList.Columns
                column.Width = Convert.ToInt32(Math.Round(column.Width * growthFactor))
            Next
        End If
    End Sub

    'Private Sub NotifyNow(theEventText As String)
    '    NtiSystray.Visible = True
    '    NtiSystray.ShowBalloonTip(100, "Info : ", theEventText, ToolTipIcon.Info)
    '    NtiSystray.Visible = False
    'End Sub

#End Region

#Region " NAVIGATION METHODS "

    Private Sub UcMainMenu1_Click(sender As Object, e As EventArgs) Handles UcMainMenu1.OnMainMenuButtonClick
        TbcMain.SelectedIndex = CInt(sender.Tag)
    End Sub

    Private Sub TpManager_Enter(sender As Object, e As EventArgs) Handles TpMainOptions.Enter, TpMainList.Enter, TpMainAbout.Enter
        Select Case ((TryCast(sender, TabPage)).Tag).ToString
            Case 0
                Text = "CapEdit"
            Case 1
                Text = "Options de CapEdit"
                Exit Select
            Case 2
                Text = "À propos de CapEdit"
        End Select
    End Sub

#End Region

#Region " CAPTURE METHODS "

    Private Sub OnCaptureNowButton_Click(sender As Object, e As EventArgs)
        If m_editFile Then
            MessageBox.Show("Une modification de capture est en cours, veuillez enregistrer et/ou fermer votre programme d'édition d'images !",
                            "Capture annulée", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            WindowState = FormWindowState.Minimized
            NtiSystray.Visible = False
            CaptureNow()
        End If
    End Sub

    Private Sub OnEditButton_Click(sender As Object, e As EventArgs)
        If m_parameters.Options.EditorDefault Then
            m_editFile = True
            WindowState = FormWindowState.Minimized

            Using edit As New FrmEdit(m_ucList.ScreenBitmapFile, m_ucList.ScreenBitmap, m_parameters)
                If edit.ShowDialog() = DialogResult.Yes Then
                    m_ucList.ScreenBitmap = edit.ModifiedImage
                    Utils.AutosizeImg(m_ucList.ScreenBitmapFile, m_ucList.PcbPictureList)
                    m_ucList.ListviewList.SelectedItems(0).SubItems(2).Text = Utils.BytestoString(New FileInfo(m_ucList.ScreenBitmapFile).Length)
                    m_clipboardImg = True
                End If
            End Using

            m_editFile = False
            WindowState = FormWindowState.Normal
            m_clipboardImg = False
        Else
            EditPicture()
        End If
    End Sub

    Private Sub OnGifAnimationText_Changed(sender As Object, txt As String)
        Me.Text = txt
    End Sub

    Private Sub CaptureActiveWindow()
        Try
            CapScreen = New CaptureScreen(m_parameters.Options.CaptureExtension, m_parameters.Options.JpegCompression)
            CapScreen.Shot(CaptureScreen.ShotType.Active)
            AddHandler CapScreen.CaptureTerminated, AddressOf CaptureTerminated
        Catch ex As Exception
        Finally
            If Not CapScreen Is Nothing Then CapScreen.Dispose()
        End Try
    End Sub

    Private Sub CaptureFullScreen(Monitor As Screen)
        Try
            CapScreen = New CaptureScreen(m_parameters.Options.CaptureExtension, m_parameters.Options.JpegCompression)
            If Monitor Is Nothing Then
                CapScreen.Shot(CaptureScreen.ShotType.Full)
            Else
                CapScreen.Shot(CaptureScreen.ShotType.SelectedMonitor, Monitor)
            End If
            AddHandler CapScreen.CaptureTerminated, AddressOf CaptureTerminated
        Catch ex As Exception
        Finally
            If Not CapScreen Is Nothing Then CapScreen.Dispose()
        End Try
    End Sub

    Private Function CaptureNow() As String
        Try
            CapScreen = New CaptureScreen(m_parameters.Options.CaptureExtension, m_parameters.Options.JpegCompression)
            CapScreen.Shot(CaptureScreen.ShotType.SelectedArea)
            AddHandler CapScreen.CaptureTerminated, AddressOf CaptureTerminated
        Catch ex As Exception
        Finally
            If Not CapScreen Is Nothing Then CapScreen.Dispose()
        End Try
        Return Nothing
    End Function

    Private Sub CaptureTerminated(sender As Object, e As EventArgs) Handles CapScreen.CaptureTerminated
        m_ucList.ScreenBitmapFile = TryCast(sender, CaptureScreen).ScreenPath
        m_ucList.ScreenBitmap = TryCast(sender, CaptureScreen).ScreenBitmap
        Dim flag As Boolean = Not m_ucList.PcbPictureList.Image Is Nothing
        NtiSystray.Visible = False
        m_ucList.TsBtnEditList.Enabled = flag
        m_ucList.TsBtnRemoveFileList.Enabled = flag
        m_ucList.TsBtnSaveAsList.Enabled = flag
        m_ucList.TsBtnAddFileList.Enabled = True
        TbcMain.SelectedIndex = 0

        If m_parameters.Options.AfterCapture Then
            If m_ucList.ScreenBitmap Is Nothing Then
                'm_hideNotify = False
            Else
                m_clipboardImg = True
            End If
            NtiSystray.Visible = True
            WindowState = FormWindowState.Minimized
        Else
            WindowState = FormWindowState.Normal
        End If
        If Not m_ucList.BgwFilesAdd.IsBusy Then
            m_ucList.BgwFilesAdd.RunWorkerAsync(m_ucList.ScreenBitmapFile)
        End If
        RemoveHandler CapScreen.CaptureTerminated, AddressOf CaptureTerminated
    End Sub

#End Region

#Region " SYSTRAY "
    Private Sub NtiSystray_MouseClick(sender As Object, e As MouseEventArgs) Handles NtiSystray.MouseClick
        If Not m_editFile Then
            If e.Button = Windows.Forms.MouseButtons.Left Then
                If WindowState = FormWindowState.Minimized Then
                    WindowState = FormWindowState.Normal
                ElseIf WindowState = FormWindowState.Normal Then
                    WindowState = FormWindowState.Minimized
                End If
            End If
        End If
    End Sub

    Private Sub TsmFrmMainShowing_Click(sender As Object, e As EventArgs) Handles TsmFrmMainShowing.Click
        If m_editFile Then
            EditInProgress()
        Else
            WindowState = FormWindowState.Normal : TbcMain.SelectedIndex = 0
        End If
    End Sub

    Private Sub TsmCaptureFullscreen_Click(sender As Object, e As EventArgs) Handles TsmCaptureFullscreen.Click, TsmCaptureActiveScreen.Click, TsmCaptureMonitor1.Click, TsmCaptureMonitor2.Click, TsmCaptureMonitor3.Click, TsmFrmOptionsShowing.Click
        If m_editFile Then
            EditInProgress()
        Else
            Dim tsmu As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
            If tsmu Is TsmCaptureFullscreen Then
                CaptureFullScreen(Nothing)
            ElseIf tsmu Is TsmCaptureActiveScreen Then
                CaptureNow()
            ElseIf tsmu Is TsmCaptureMonitor1 OrElse tsmu Is TsmCaptureMonitor2 OrElse tsmu Is TsmCaptureMonitor3 Then
                CaptureFullScreen(TryCast(TryCast(sender, ToolStripMenuItem).Tag, Screen))
            ElseIf tsmu Is TsmFrmOptionsShowing Then
                WindowState = FormWindowState.Normal : TbcMain.SelectedTab = TpMainOptions
            End If
        End If
    End Sub

    Private Sub TsmClosingApp_Click(sender As Object, e As EventArgs) Handles TsmClosingApp.Click
        Application.Exit()
    End Sub

    Private Sub EditInProgress()
        MessageBox.Show("Une modification de capture est en cours, veuillez enregistrer et/ou fermer votre programme d'édition d'images !",
                        "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub EditPicture()
        Dim p As Killer = Nothing
        Try
            Dim editorPath = m_parameters.Options.EditApp
            p = New Killer(editorPath)
            If p.isRunning Then
                If MessageBox.Show("Toutes les instances de votre programme d'édition d'images vont être fermées, souhaitez-vous continuer ?",
                                   "Fermeture de programme", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
                    If Not p.killpTree() Then
                        MessageBox.Show("Une erreur est survenue, veuillez fermer manuellement vos programmes et appuyer de nouveau sur le bouton d'édition !",
                                        "Fermeture de programme", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    Else
                        EditorOpen(editorPath)
                    End If
                Else
                    Exit Sub
                End If
            Else
                EditorOpen(editorPath)
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)
        Finally
            If Not m_edit Is Nothing Then m_edit.Dispose()
            If Not p Is Nothing Then p.Dispose()
        End Try
    End Sub

    Private Sub EditorOpen(editorPath As String)
        m_edit = New Watcher
        m_edit.startWatch(editorPath, m_ucList.ScreenBitmapFile)
        m_edit.Started = New Watcher.StartedEventHandler(AddressOf EditStarted)
        m_edit.Stopped = New Watcher.StoppedEventHandler(AddressOf EditStopped)
    End Sub

    Private Sub EditStarted(sender As Object, e As EventArgs)
        m_editFile = True
        m_edit.Started = Nothing
        m_edit.Terminated = New Watcher.TerminatedEventHandler(AddressOf Me.EditTerminated)
        If InvokeRequired Then
            Invoke(New HiddingMainForm(AddressOf Frm_Main_Hidding))
        End If
    End Sub

    Private Sub Frm_Main_Hidding()
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub EditStopped(sender As Object, e As EventArgs)
        m_editFile = False
        If Not m_edit Is Nothing Then
            m_edit.Dispose()
        End If
        If InvokeRequired Then
            Invoke(New ShowingMainForm(AddressOf Frm_Main_Showing))
        End If
    End Sub

    Private Sub Frm_Main_Showing()
        Try
            If Utils.GetMIMEType(m_ucList.ScreenBitmapFile) = "image/gif" Then
                m_ucList.ScreenBitmap = Utils.GetGifFirstFrame(m_ucList.ScreenBitmapFile)
            Else
                m_ucList.ScreenBitmap = Utils.ImageFromFile(m_ucList.ScreenBitmapFile)
            End If
            Utils.AutosizeImg(m_ucList.ScreenBitmapFile, m_ucList.PcbPictureList)
            m_ucList.ListviewList.SelectedItems(0).SubItems(2).Text = Utils.BytestoString(New FileInfo(m_ucList.ScreenBitmapFile).Length)
        Catch ex As Exception
        End Try
        TbcMain.SelectedIndex = 0
        WindowState = FormWindowState.Normal
        ShowInTaskbar = True
    End Sub

    Private Sub EditTerminated(sender As Object, e As EventArgs)
        m_edit.Terminated = Nothing
        m_edit.StopWatch()
    End Sub

#End Region

End Class
