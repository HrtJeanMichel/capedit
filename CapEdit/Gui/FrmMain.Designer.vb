﻿Imports CapEdit.Controls
Imports CapEdit.Win32

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits Form

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = NativeConstants.WM_HOTKEY Then
            If Not m_editFile Then
                If WindowState = FormWindowState.Minimized Then
                    If m_parameters.Options.ActiveWindow Then
                        CaptureActiveWindow()
                    Else
                        CaptureFullScreen(Nothing)
                    End If
                End If
            End If
        End If
        MyBase.WndProc(m)
    End Sub

    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    Private components As System.ComponentModel.IContainer

    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.ImgListOptions = New System.Windows.Forms.ImageList(Me.components)
        Me.NtiSystray = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.CmsSystray = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TsmFrmMainShowing = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmSystraySep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsmCaptureFullscreen = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmCaptureActiveScreen = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmCaptureMonitor1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmCaptureMonitor2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmCaptureMonitor3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmSystraySep2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsmFrmOptionsShowing = New System.Windows.Forms.ToolStripMenuItem()
        Me.TsmSystraySep3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TsmClosingApp = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.TtImaboxBan = New System.Windows.Forms.ToolTip(Me.components)
        Me.SpcMainHorizontal = New System.Windows.Forms.SplitContainer()
        Me.UcMainMenu1 = New CapEdit.Controls.UcMainMenu()
        Me.TbcMain = New System.Windows.Forms.TabControl()
        Me.TpMainList = New System.Windows.Forms.TabPage()
        Me.TpMainOptions = New System.Windows.Forms.TabPage()
        Me.TpMainAbout = New System.Windows.Forms.TabPage()
        Me.CmsSystray.SuspendLayout()
        CType(Me.SpcMainHorizontal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcMainHorizontal.Panel1.SuspendLayout()
        Me.SpcMainHorizontal.Panel2.SuspendLayout()
        Me.SpcMainHorizontal.SuspendLayout()
        Me.TbcMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImgListOptions
        '
        Me.ImgListOptions.ImageStream = CType(resources.GetObject("ImgListOptions.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImgListOptions.TransparentColor = System.Drawing.Color.Transparent
        Me.ImgListOptions.Images.SetKeyName(0, "menu.png")
        '
        'NtiSystray
        '
        Me.NtiSystray.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.NtiSystray.BalloonTipText = "CapEdit est içi !"
        Me.NtiSystray.BalloonTipTitle = "Info :"
        Me.NtiSystray.ContextMenuStrip = Me.CmsSystray
        Me.NtiSystray.Icon = CType(resources.GetObject("NtiSystray.Icon"), System.Drawing.Icon)
        '
        'CmsSystray
        '
        Me.CmsSystray.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.CmsSystray.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TsmFrmMainShowing, Me.TsmSystraySep1, Me.TsmCaptureFullscreen, Me.TsmCaptureActiveScreen, Me.TsmCaptureMonitor1, Me.TsmCaptureMonitor2, Me.TsmCaptureMonitor3, Me.TsmSystraySep2, Me.TsmFrmOptionsShowing, Me.TsmSystraySep3, Me.TsmClosingApp})
        Me.CmsSystray.Name = "ContextMenuStrip1"
        Me.CmsSystray.Size = New System.Drawing.Size(209, 262)
        '
        'TsmFrmMainShowing
        '
        Me.TsmFrmMainShowing.Image = Global.CapEdit.My.Resources.Resources.MainWindow
        Me.TsmFrmMainShowing.Name = "TsmFrmMainShowing"
        Me.TsmFrmMainShowing.Size = New System.Drawing.Size(208, 30)
        Me.TsmFrmMainShowing.Tag = ""
        Me.TsmFrmMainShowing.Text = "Fenêtre principale"
        '
        'TsmSystraySep1
        '
        Me.TsmSystraySep1.Name = "TsmSystraySep1"
        Me.TsmSystraySep1.Size = New System.Drawing.Size(205, 6)
        '
        'TsmCaptureFullscreen
        '
        Me.TsmCaptureFullscreen.Image = Global.CapEdit.My.Resources.Resources.captureScreen
        Me.TsmCaptureFullscreen.Name = "TsmCaptureFullscreen"
        Me.TsmCaptureFullscreen.Size = New System.Drawing.Size(208, 30)
        Me.TsmCaptureFullscreen.Tag = ""
        Me.TsmCaptureFullscreen.Text = "Capturer tout l'écran"
        '
        'TsmCaptureActiveScreen
        '
        Me.TsmCaptureActiveScreen.Image = Global.CapEdit.My.Resources.Resources.AreaSelect
        Me.TsmCaptureActiveScreen.Name = "TsmCaptureActiveScreen"
        Me.TsmCaptureActiveScreen.Size = New System.Drawing.Size(208, 30)
        Me.TsmCaptureActiveScreen.Tag = ""
        Me.TsmCaptureActiveScreen.Text = "Définir nouvelle capture"
        '
        'TsmCaptureMonitor1
        '
        Me.TsmCaptureMonitor1.Image = Global.CapEdit.My.Resources.Resources.captureScreen
        Me.TsmCaptureMonitor1.Name = "TsmCaptureMonitor1"
        Me.TsmCaptureMonitor1.Size = New System.Drawing.Size(208, 30)
        Me.TsmCaptureMonitor1.Tag = ""
        Me.TsmCaptureMonitor1.Text = "Capturer moniteur 1"
        Me.TsmCaptureMonitor1.Visible = False
        '
        'TsmCaptureMonitor2
        '
        Me.TsmCaptureMonitor2.Image = Global.CapEdit.My.Resources.Resources.captureScreen
        Me.TsmCaptureMonitor2.Name = "TsmCaptureMonitor2"
        Me.TsmCaptureMonitor2.Size = New System.Drawing.Size(208, 30)
        Me.TsmCaptureMonitor2.Tag = ""
        Me.TsmCaptureMonitor2.Text = "Capturer moniteur 2"
        Me.TsmCaptureMonitor2.Visible = False
        '
        'TsmCaptureMonitor3
        '
        Me.TsmCaptureMonitor3.Image = Global.CapEdit.My.Resources.Resources.captureScreen
        Me.TsmCaptureMonitor3.Name = "TsmCaptureMonitor3"
        Me.TsmCaptureMonitor3.Size = New System.Drawing.Size(208, 30)
        Me.TsmCaptureMonitor3.Tag = ""
        Me.TsmCaptureMonitor3.Text = "Capturer moniteur 3"
        Me.TsmCaptureMonitor3.Visible = False
        '
        'TsmSystraySep2
        '
        Me.TsmSystraySep2.Name = "TsmSystraySep2"
        Me.TsmSystraySep2.Size = New System.Drawing.Size(205, 6)
        '
        'TsmFrmOptionsShowing
        '
        Me.TsmFrmOptionsShowing.Image = Global.CapEdit.My.Resources.Resources.options
        Me.TsmFrmOptionsShowing.Name = "TsmFrmOptionsShowing"
        Me.TsmFrmOptionsShowing.Size = New System.Drawing.Size(208, 30)
        Me.TsmFrmOptionsShowing.Text = "Options"
        '
        'TsmSystraySep3
        '
        Me.TsmSystraySep3.Name = "TsmSystraySep3"
        Me.TsmSystraySep3.Size = New System.Drawing.Size(205, 6)
        '
        'TsmClosingApp
        '
        Me.TsmClosingApp.Image = Global.CapEdit.My.Resources.Resources.Close
        Me.TsmClosingApp.Name = "TsmClosingApp"
        Me.TsmClosingApp.Size = New System.Drawing.Size(208, 30)
        Me.TsmClosingApp.Text = "Fermer"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(6, 25)
        '
        'SpcMainHorizontal
        '
        Me.SpcMainHorizontal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcMainHorizontal.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SpcMainHorizontal.IsSplitterFixed = True
        Me.SpcMainHorizontal.Location = New System.Drawing.Point(0, 0)
        Me.SpcMainHorizontal.Name = "SpcMainHorizontal"
        Me.SpcMainHorizontal.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SpcMainHorizontal.Panel1
        '
        Me.SpcMainHorizontal.Panel1.Controls.Add(Me.UcMainMenu1)
        '
        'SpcMainHorizontal.Panel2
        '
        Me.SpcMainHorizontal.Panel2.Controls.Add(Me.TbcMain)
        Me.SpcMainHorizontal.Size = New System.Drawing.Size(664, 499)
        Me.SpcMainHorizontal.SplitterDistance = 25
        Me.SpcMainHorizontal.TabIndex = 1
        '
        'UcMainMenu1
        '
        Me.UcMainMenu1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcMainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.UcMainMenu1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.UcMainMenu1.Name = "UcMainMenu1"
        Me.UcMainMenu1.Size = New System.Drawing.Size(664, 25)
        Me.UcMainMenu1.TabIndex = 0
        '
        'TbcMain
        '
        Me.TbcMain.Controls.Add(Me.TpMainList)
        Me.TbcMain.Controls.Add(Me.TpMainOptions)
        Me.TbcMain.Controls.Add(Me.TpMainAbout)
        Me.TbcMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TbcMain.Location = New System.Drawing.Point(0, 0)
        Me.TbcMain.Name = "TbcMain"
        Me.TbcMain.SelectedIndex = 0
        Me.TbcMain.Size = New System.Drawing.Size(664, 470)
        Me.TbcMain.TabIndex = 0
        '
        'TpMainList
        '
        Me.TpMainList.Location = New System.Drawing.Point(4, 22)
        Me.TpMainList.Name = "TpMainList"
        Me.TpMainList.Padding = New System.Windows.Forms.Padding(3)
        Me.TpMainList.Size = New System.Drawing.Size(656, 444)
        Me.TpMainList.TabIndex = 5
        Me.TpMainList.Tag = "0"
        Me.TpMainList.Text = "TabPage1"
        Me.TpMainList.UseVisualStyleBackColor = True
        '
        'TpMainOptions
        '
        Me.TpMainOptions.Location = New System.Drawing.Point(4, 22)
        Me.TpMainOptions.Name = "TpMainOptions"
        Me.TpMainOptions.Padding = New System.Windows.Forms.Padding(3)
        Me.TpMainOptions.Size = New System.Drawing.Size(594, 444)
        Me.TpMainOptions.TabIndex = 4
        Me.TpMainOptions.Tag = "1"
        Me.TpMainOptions.Text = "TabPage3"
        Me.TpMainOptions.UseVisualStyleBackColor = True
        '
        'TpMainAbout
        '
        Me.TpMainAbout.Location = New System.Drawing.Point(4, 22)
        Me.TpMainAbout.Name = "TpMainAbout"
        Me.TpMainAbout.Padding = New System.Windows.Forms.Padding(3)
        Me.TpMainAbout.Size = New System.Drawing.Size(594, 444)
        Me.TpMainAbout.TabIndex = 6
        Me.TpMainAbout.Tag = "2"
        Me.TpMainAbout.Text = "TabPage4"
        Me.TpMainAbout.UseVisualStyleBackColor = True
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(664, 499)
        Me.Controls.Add(Me.SpcMainHorizontal)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CapEdit"
        Me.CmsSystray.ResumeLayout(False)
        Me.SpcMainHorizontal.Panel1.ResumeLayout(False)
        Me.SpcMainHorizontal.Panel2.ResumeLayout(False)
        CType(Me.SpcMainHorizontal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcMainHorizontal.ResumeLayout(False)
        Me.TbcMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SpcMainHorizontal As System.Windows.Forms.SplitContainer
    Friend WithEvents NtiSystray As System.Windows.Forms.NotifyIcon
    Friend WithEvents CmsSystray As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TsmFrmMainShowing As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmSystraySep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TsmCaptureFullscreen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmCaptureActiveScreen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmSystraySep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TsmFrmOptionsShowing As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmSystraySep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TsmClosingApp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImgListOptions As System.Windows.Forms.ImageList
    Friend WithEvents TsmCaptureMonitor1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TsmCaptureMonitor2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Cpt_ListViewEx1 As CapEdit.Controls.ListviewEx
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TtImaboxBan As System.Windows.Forms.ToolTip
    Friend WithEvents TbcMain As TabControl
    Friend WithEvents TpMainList As TabPage
    Friend WithEvents TpMainOptions As TabPage
    Friend WithEvents TpMainAbout As TabPage
    Friend WithEvents TsmCaptureMonitor3 As ToolStripMenuItem
    Friend WithEvents UcMainMenu1 As UcMainMenu
End Class
