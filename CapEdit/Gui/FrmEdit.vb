﻿Imports System.Collections.Specialized
Imports System.Drawing.Drawing2D
Imports System.IO
Imports CapEdit.Core.Editor
Imports CapEdit.Helpers.Pictures
Imports CapEdit.Settings

Public Class FrmEdit
    Implements IDisposable


#Region " Fields "
    Private ReadOnly m_originalImage As Bitmap

    Private m_RectImage As Bitmap
    Private m_penColor As Color
    Private m_penSize As PenSize
    Private m_startPos As Point
    Private m_currentPos As Point
    Private m_drawing As Boolean
    Private WithEvents Figures As Shapes(Of Shape)
    Private ReadOnly m_settings As Parameters
    Private ReadOnly m_FilePath As String
    Public ModifiedImage As Bitmap

    Private m_lastLoc As Point
    Private m_lastSize As Size
    Private m_mouseDownPoint As Point = Point.Empty
    Private m_mousePoint As Point = Point.Empty
    Private ReadOnly m_penCrop As Pen
    Private m_Makeselection As Boolean
    Private m_CroppedImage As Bitmap
    Private m_Cropped As Boolean
    Private m_cropdraw As Boolean
    Private WithEvents Bitmaps As Shapes(Of Bitmap)

    Private m_Painting As Boolean

#End Region

#Region " Constructor "
    Public Sub New(fpath As String, bImage As Bitmap, ByRef Settings As Parameters)
        m_settings = Settings
        InitializeComponent()
        MyBase.DoubleBuffered = True
        UcColor1.Initialize(Settings.Options.EditorPenColor)
        AddHandler UcColor1.OnColorChanged, AddressOf UcColor1_OnColorChanged

        m_FilePath = fpath
        m_originalImage = bImage
        m_RectImage = New Bitmap(bImage)

        TsBtnRectangle.Checked = True
        TsBtnRectangle.Enabled = False
        TsBtnRectangleCrop.Checked = False
        TsBtnRectangleCrop.Enabled = True

        TsCbxPenSize.SelectedIndex = Settings.Options.EditorPenSize - 1
        m_penColor = Color.FromArgb(Settings.Options.EditorPenColor)

        m_penCrop = New Pen(Color.FromArgb(Settings.Options.EditorPenColor), 1) With {
            .DashStyle = DashStyle.Dot
        }

        Figures = New Shapes(Of Shape)
        PcbEdit.BackgroundImage = m_RectImage
        PcbEdit.Size = PcbEdit.BackgroundImage.Size

        Bitmaps = New Shapes(Of Bitmap)
    End Sub
#End Region

#Region " Methods "
    Private Sub TsBtnUndo_Click(sender As Object, e As EventArgs) Handles TsBtnUndo.Click
        m_drawing = False
        If TsBtnRectangle.Checked Then
            If Figures.Count > 0 Then
                PcbEdit.BackgroundImage = m_RectImage
                Figures.RemoveAt(Figures.Count - 1)
                PcbEdit.Invalidate()
            End If
        End If
        If TsBtnRectangleCrop.Checked Then
            If Bitmaps.Count > 0 Then
                Dim bmp = New Bitmap(Bitmaps.Item(Bitmaps.Count - 1))
                PcbEdit.BackgroundImage = bmp
                PcbEdit.Size = bmp.Size
                Bitmaps.RemoveAt(Bitmaps.Count - 1)
            End If
        End If
    End Sub

    Private Sub Figures_ItemAdded(sender As Object, e As NotifyCollectionChangedEventArgs) Handles Figures.ItemAdded
        If Figures.Count > 0 Then TsBtnUndo.Enabled = True
    End Sub

    Private Sub Figures_ItemRemovedOrReset(sender As Object, e As NotifyCollectionChangedEventArgs) Handles Figures.ItemRemoved, Figures.ItemReset
        If Figures.Count = 0 Then TsBtnUndo.Enabled = False
    End Sub

    Private Sub Bitmaps_ItemAdded(sender As Object, e As NotifyCollectionChangedEventArgs) Handles Bitmaps.ItemAdded
        If Bitmaps.Count > 0 Then TsBtnUndo.Enabled = True
    End Sub

    Private Sub Bitmaps_ItemRemovedOrReset(sender As Object, e As NotifyCollectionChangedEventArgs) Handles Bitmaps.ItemRemoved, Bitmaps.ItemReset
        If Bitmaps.Count = 0 Then TsBtnUndo.Enabled = False
    End Sub

    Private Function GetRectangle() As Rectangle
        Return New Rectangle(Math.Min(m_startPos.X, m_currentPos.X), Math.Min(m_startPos.Y, m_currentPos.Y), Math.Abs(m_startPos.X - m_currentPos.X), Math.Abs(m_startPos.Y - m_currentPos.Y))
    End Function

    Private Sub UcColor1_OnColorChanged(sender As Object, col As Color)
        m_penColor = col
        m_penCrop.Color = col
        m_settings.Options.EditorPenColor = m_penColor.ToArgb.ToString
        m_settings.SaveFile()
    End Sub

    Private Sub PcbEdit_MouseDown(sender As Object, e As MouseEventArgs) Handles PcbEdit.MouseDown
        If e.Button = MouseButtons.Left Then
            m_currentPos = InlineAssignHelper(m_startPos, e.Location)
            If m_Cropped Then
                m_Makeselection = True
                m_mousePoint = InlineAssignHelper(m_mouseDownPoint, e.Location)
            End If

            If TsBtnRectangle.Checked Then
                m_drawing = True
            Else
                m_drawing = False
            End If
        End If
    End Sub

    Private Sub PcbEdit_MouseMove(sender As Object, e As MouseEventArgs) Handles PcbEdit.MouseMove
        m_currentPos = e.Location
        If e.Button = MouseButtons.Left Then

            If m_Cropped AndAlso m_Makeselection Then
                Cursor = Cursors.Cross
                If PcbEdit.BackgroundImage Is Nothing Then
                    Return
                End If
                m_mousePoint = e.Location
                m_cropdraw = True
            Else
                m_cropdraw = False
            End If

            If m_drawing Then
                PcbEdit.Invalidate()
            End If
        Else
            m_cropdraw = True
        End If
    End Sub

    Private Sub PcbEdit_Paint(sender As Object, e As PaintEventArgs) Handles PcbEdit.Paint
        If m_Painting = False Then
            If Figures.Count > 0 Then
                For Each gpath In Figures
                    e.Graphics.DrawPath(New Pen(gpath.Color, gpath.PenSize), gpath.GraphicsPath)
                Next
            End If

            If m_drawing Then
                Dim Gp As GraphicsPath = DrawRectangles()
                e.Graphics.DrawPath(New Pen(m_penColor, m_penSize), Gp)
            End If

            If m_Cropped AndAlso m_Makeselection Then
                Dim selectionWindow As New Rectangle(Math.Min(m_mouseDownPoint.X, m_mousePoint.X), Math.Min(m_mouseDownPoint.Y, m_mousePoint.Y), Math.Abs(m_mouseDownPoint.X - m_mousePoint.X), Math.Abs(m_mouseDownPoint.Y - m_mousePoint.Y))
                e.Graphics.DrawRectangle(m_penCrop, selectionWindow)
            End If

            If m_cropdraw Then
                PcbEdit.Invalidate()
            End If
        End If
    End Sub

    Private Sub TsBtnRectangle_Click(sender As Object, e As EventArgs) Handles TsBtnRectangle.Click
        If TsBtnRectangle.Enabled Then

            TsBtnRectangle.Checked = True
            TsBtnRectangle.Enabled = False
            TsBtnRectangleCrop.Checked = False
            TsBtnRectangleCrop.Enabled = True

            Using b = New Bitmap(PcbEdit.Width, PcbEdit.Height)
                PcbEdit.DrawToBitmap(b, New Rectangle(0, 0, b.Width, b.Height))
                m_RectImage = New Bitmap(b)
                PcbEdit.BackgroundImage.Dispose()
                PcbEdit.BackgroundImage = m_RectImage
            End Using

            m_Cropped = False

            Bitmaps.Clear()
        End If
    End Sub

    Private Sub TsBtnRectangleCrop_Click(sender As Object, e As EventArgs) Handles TsBtnRectangleCrop.Click
        If TsBtnRectangleCrop.Enabled Then
            TsBtnRectangle.Checked = False
            TsBtnRectangle.Enabled = True
            TsBtnRectangleCrop.Checked = True
            TsBtnRectangleCrop.Enabled = False

            Using b = New Bitmap(PcbEdit.Width, PcbEdit.Height)
                PcbEdit.DrawToBitmap(b, New Rectangle(0, 0, b.Width, b.Height))
                m_CroppedImage = New Bitmap(b)
                PcbEdit.BackgroundImage.Dispose()
                PcbEdit.BackgroundImage = m_CroppedImage
            End Using

            m_drawing = False
            m_Cropped = True
            Figures.Clear()
        End If
    End Sub

    Private Sub PcbEdit_MouseUp(sender As Object, e As MouseEventArgs) Handles PcbEdit.MouseUp
        If m_drawing Then
            m_drawing = False
            Dim Gp As GraphicsPath = DrawRectangles()
            If Gp.GetBounds.Width > 0 AndAlso Gp.GetBounds.Height > 0 Then
                Figures.Add(New Shape(m_penColor, m_penSize, Gp))
            End If
            PcbEdit.Invalidate()
        End If
        If m_Cropped AndAlso m_Makeselection Then
            Cursor = Cursors.Default
            Try
                Me.m_lastLoc = New Point(Math.Min(m_mouseDownPoint.X, m_mousePoint.X), Math.Min(m_mouseDownPoint.Y, m_mousePoint.Y))
                Me.m_lastSize = New Size(Math.Abs(m_mouseDownPoint.X - m_mousePoint.X), Math.Abs(m_mouseDownPoint.Y - m_mousePoint.Y))
                If m_lastSize.Width > 0 AndAlso m_lastSize.Height > 0 Then
                    Dim rect As New Rectangle With {
                        .Location = m_lastLoc,
                        .Size = m_lastSize
                    }

                    Dim origImage = New Bitmap(PcbEdit.BackgroundImage, PcbEdit.Width, PcbEdit.Height)

                    Bitmaps.Add(origImage)

                    Dim _img As Bitmap = New Bitmap(rect.Width, rect.Height)
                    Using g As Graphics = Graphics.FromImage(_img)
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality
                        g.CompositingQuality = CompositingQuality.HighQuality
                        g.DrawImage(origImage, 0, 0, rect, GraphicsUnit.Pixel)
                        PcbEdit.BackgroundImage.Dispose()
                        PcbEdit.BackgroundImage = _img
                        PcbEdit.Size = _img.Size
                        m_CroppedImage = _img
                    End Using
                End If
                m_Makeselection = False
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub

    Private Function DrawRectangles() As GraphicsPath
        Dim Gp As GraphicsPath = New GraphicsPath
        Gp.AddRectangle(GetRectangle())
        Return Gp
    End Function

    Private Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
        target = value
        Return value
    End Function

    Private Sub CbxPenSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TsCbxPenSize.SelectedIndexChanged
        m_penSize = TsCbxPenSize.SelectedIndex + 1
        m_settings.Options.EditorPenSize = TsCbxPenSize.SelectedIndex + 1
        m_settings.SaveFile()
    End Sub

    Private Sub FrmEdit_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Using b = New Bitmap(PcbEdit.Width, PcbEdit.Height)
            PcbEdit.DrawToBitmap(b, New Rectangle(0, 0, b.Width, b.Height))
            ModifiedImage = New Bitmap(b)
            Utils.SavePicture(b, m_FilePath, Path.GetExtension(m_FilePath).ToLower(), m_settings.Options.JpegCompression)
        End Using
        Me.DialogResult = DialogResult.Yes
    End Sub

    Private Sub TsBtnReload_Click(sender As Object, e As EventArgs) Handles TsBtnReload.Click
        m_Painting = True
        Dim dlgResult = MessageBox.Show("Souhaitez-vous recharger l'image par défaut ?", "Recharger", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If dlgResult = DialogResult.Yes Then
            TsBtnRectangle.Checked = True
            TsBtnRectangle.Enabled = False
            TsBtnRectangleCrop.Checked = False
            TsBtnRectangleCrop.Enabled = True

            m_Cropped = False
            m_RectImage = New Bitmap(m_originalImage)

            PcbEdit.BackgroundImage.Dispose()
            PcbEdit.BackgroundImage = m_RectImage
            PcbEdit.Size = m_originalImage.Size

            Bitmaps.Clear()
            Figures.Clear()
        End If
        m_Painting = False
    End Sub

#End Region

End Class